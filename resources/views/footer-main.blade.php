 <div class="footer-bottom">
        <section id="bottom">
            <div class="container wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                <div class="row">
                    <div class="col-md-4 col-sm-4">
                        <div class="widget">
                            <h3>Useful Links</h3>
                            <ul>
                                <li><a href="{{url('about')}}"><i class="fa fa-user" aria-hidden="true"></i> About Us</a></li>
                                <li><a href="{{url('contact')}}"><i class="fa fa-phone" aria-hidden="true"></i> Contact</a></li>
                                <li><a href="{{url('terms')}}"><i class="fa fa-info" aria-hidden="true"></i> Terms & Conditions</a></li>
                            </ul>
                        </div>    
                    </div><!--/.col-md-3-->

                    <div class="col-md-4 col-sm-4">
                        <div class="widget">
                            <h3>Some Links</h3>
                            <ul>
                                <li><a href="{{url('how-to-make-money')}}"><i class="fa fa-money" aria-hidden="true"></i> Make Money</a></li>
                                <li><a href="{{url('privacy')}}"><i class="fa fa-lock" aria-hidden="true"></i> Privacy & Policy</a></li>
                                <li><a href="{{url('#')}}"><i class="fa fa-question" aria-hidden="true"></i> FAQS</a></li>
                            </ul>
                        </div>    
                    </div><!--/.col-md-3-->

                    <div class="col-md-4 col-sm-4">
                        <div class="widget socials">
                            <h3>Connect With Us</h3>
                            <p></p>
                            <p><a href=""></a></p>
                            <ul>
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                            </ul>
                        </div>    
                    </div><!--/.col-md-3-->
                </div>
            </div>
        </section><!--/#bottom-->
        <hr>
        <footer id="footer" class="midnight-blue">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 copyright">
                        Copyright &copy; {{date('Y')}} <a target="_blank" href="{{url('/')}}" title="">ZeroPovertyng</a>. All Rights Reserved.
                    </div>
                </div>
            </div>
        </footer><!--/#footer-->
    </div>
