
        <div class="side-content-wrap">
            <div class="sidebar-left open" data-perfect-scrollbar data-suppress-scroll-x="true">
                <ul class="navigation-left">
                    <li class="nav-item active">
                        <a class="nav-item-hold" href="{{ url('users/dashboard') }}">
                            <i class="nav-icon i-Bar-Chart"></i>
                            <span class="nav-text">Dashboard</span>
                        </a>
                        <div class="triangle"></div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-item-hold" href="{{ url('users/timeline') }}">
                            <i class="nav-icon i-Library"></i>
                            <span class="nav-text">My Profile</span>
                        </a>
                        <div class="triangle"></div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-item-hold" href="{{ url('users/post/new') }}">
                            <i class="nav-icon i-Library"></i>
                            <span class="nav-text">Create Post</span>
                        </a>
                        <div class="triangle"></div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-item-hold" href="{{ url('users/password') }}">
                            <i class="nav-icon i-Library"></i>
                            <span class="nav-text">Change Password</span>
                        </a>
                        <div class="triangle"></div>
                    </li>
                    <li class="nav-item" data-item="affiliate">
                        <a class="nav-item-hold" href="#">
                            <i class="nav-icon i-Suitcase"></i>
                            <span class="nav-text">Affiliate</span>
                        </a>
                        <div class="triangle"></div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-item-hold" href="{{ url('users/activities') }}">
                            <i class="nav-icon i-Computer-Secure"></i>
                            <span class="nav-text">View Activities</span>
                        </a>
                        <div class="triangle"></div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-item-hold" href="{{ url('users/withdraw') }}">
                            <i class="nav-icon i-File-Horizontal-Text"></i>
                            <span class="nav-text">Withdrawal</span>
                        </a>
                        <div class="triangle"></div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-item-hold" href="{{ url('how-to-make-money') }}">
                            <i class="nav-icon i-File-Horizontal-Text"></i>
                            <span class="nav-text">How to Make Money</span>
                        </a>
                        <div class="triangle"></div>
                    </li>
                    <!-- <li class="nav-item">
                        <a class="nav-item-hold" href="{{ url('advertisement') }}">
                            <i class="nav-icon i-File-Horizontal-Text"></i>
                            <span class="nav-text">Advertisement with us</span>
                        </a>
                        <div class="triangle"></div>
                    </li> -->
                    @if(Auth::user()->package_type != 'Affiliate')
                        <li class="nav-item">
                            <a class="nav-item-hold" href="{{ url('users/upgrade') }}">
                                <i class="nav-icon i-Administrator"></i>
                                <span class="nav-text">{{ __('Upgrade Account') }}</span>
                            </a>
                            <div class="triangle"></div>
                        </li>
                    @endif
                    <li class="nav-item">
                        <a class="nav-item-hold" href="{{ url('users/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            <i class="nav-icon i-Double-Tap"></i>
                            <span class="nav-text">{{ __('Logout') }}</span>
                        </a>
                        <form id="logout-form" action="{{ url('users/logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                        <div class="triangle"></div>
                    </li>
                </ul>
            </div>

            <div class="sidebar-left-secondary" data-perfect-scrollbar data-suppress-scroll-x="true">
                <ul class="childNav" data-parent="affiliate">
                    <li class="nav-item">
                        <a href="{{ url('sponsor/post') }}">
                            <i class="nav-icon i-Crop-2"></i>
                            <span class="item-name">Sponsored Post</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ url('users/referrals') }}">
                            <i class="nav-icon i-Loading-3"></i>
                            <span class="item-name">Referral List</span>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="sidebar-overlay"></div>
        </div>
        <!--=============== Left side End ================-->
