@extends('layouts.app3')

@section('content')
@section('title')
  Home
@endsection

<div class="container" id="login" style="margin-bottom: 30px;">
	<div class="row" style="margin-bottom: 20px;">
		<div class="col-md-8 col-offset-md-3" style="text-align: center;">
			@if(!empty($categories))
			@foreach($categories as $category)
				<span><a href="{{url('forum/'. $category->slug)}}" style="text-decoration: none; font-size: 15px;">{{ $category->title }}</a></span>,
			@endforeach
			@endif
		</div>
	</div>
    <div class="row">
        <div class="col-md-8">
            <div class="panel">

                <div class="panel-body">
                    @if(!empty($pinPost))
                    @foreach($pinPost as $post)
                    	<p style="border: 1px solid #e1e1e1; padding-left: 7px; border-left: 4px solid #ff0000; padding-top: 4px; padding-bottom: 4px;">
                    		<a href="{{ url('forum/topic/'.$post->slug) }}" style="text-decoration: none;">{{ $post->title }}</a>
                            <span style="float: right; padding-right: 5px; color: #25135c"><i class="fa fa-map-pin" aria-hidden="true"></i></span>

                            <br>
                    		<span><small>By @if($post->user_id == null)
                    			{{ $post->admin->username }}
                    			@else 
                    				{{ $post->user->name }}
                    			@endif {{ $post->created_at->diffForHumans() }}</small></span>
                    	</p>

                    @endforeach
                    @endif

                    @if(!empty($sponsoredPost))
                    	<p style="border: 1px solid #e1e1e1; padding-left: 7px; border-left: 4px solid #ff0000; padding-top: 4px; padding-bottom: 4px;">
                    		<a href="{{ url('sponsor/post/'.$sponsoredPost->slug) }}" style="text-decoration: none;">{{ $sponsoredPost->title }}</a><br>
                    		<span>
                    			<small>
                                    By {{ $sponsoredPost->admin->username }}
                                    
                    				{{ $sponsoredPost->created_at->diffForHumans() }}
                    			</small>
                    		</span>
                    	</p>
                    @endif

                    @if(!empty($posts))
                    	@foreach($posts as $post)
                    	<p style="border: 1px solid #e1e1e1; padding-left: 7px; border-left: 4px solid #ff0000; padding-top: 4px; padding-bottom: 4px;">
                    		<a href="{{ url('forum/topic/'.$post->slug) }}" style="text-decoration: none;">{{ $post->title }}</a>

                                <span style="float: right; padding-right: 5px; color: #25135c"><i class="fa fa-money"></i></span>
                            <br>
                    		<span><small>By @if($post->user_id == null)
                    			{{ $post->admin->username }}
                    			@else 
                    				{{ $post->user->name }}
                    			@endif {{ $post->created_at->diffForHumans() }}</small></span>
                    	</p>

                    	@endforeach

                    	{{ $posts->links() }}
                    @else
                    	<h4>No Posts Available now</h4>
                    @endif
                </div>
            </div>
        </div>

        <div class="col-md-4" >
        	<div class="panel">
        		<!-- <div class="panel-body">
        			
        		</div> -->
        	</div>
        	<div class="panel" style="top: 80px; margin-bottom: 30px;">
        		<div class="panel-header" style="background: #8f5297; color: #fff;">
        			Quick User Earnings
        		</div>
        		<div class="panel-body">
        			@guest
        				<p> You need to be logged in to see your earnings!</p>
        			@else
        				<ul style="list-style: none;">
        					@foreach($activities as $activity)
        						<li style="font-size: 1.10em; font-weight: 700; border: 1px solid #bcb8b8; margin-bottom: 3px; padding-left: 4px;">&#8358;{{ $activity->amount_earned }} - {{ $activity->activity_type }} - {{ $activity->created_at->diffForHumans() }} </li>
        					@endforeach
        				</ul>
        			@endguest
        		</div>
        	</div>
        </div>
    </div>
</div>
@include('footer-main')
@endsection