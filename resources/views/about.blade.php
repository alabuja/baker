@extends('layouts.app3')

@section('content')
@section('title')
  About Us
@endsection
<div class="container" id="login">
    <div class="row">
        <div class="col-md-8">
            <div class="panel">

                <div class="panel-body">
                    <h3>About Us</h3>
                    <br>
                    <p><b>ZIP(ZeroPoverty Income Program)</b></p>

                    <p>Zeropoverty income program is a platform which sole aim  is to reduce the high rate of poverty and unemployment *among the youths in the country*.</p>

                    <p>Making money is not easy and working under the scorching sun is very tedious, hence, without any job, the level of poverty increases.</p>

                    <p>In Nigeria, Findings has shown that the level of poverty has risen with 46.7% which means that not less than 86.5 million people are wallowing in abject poverty in the country.</p>

                    <br>

                    <p>It *is* also on record that more than 60 million Nigeria youths are either unemployed or under employed.</p>
                    <br>
                    <p>In the process of reducing the level of poverty and unemployment rate in the country, ZeroPoverty.ng Income Program (ZIP) *is* therefore *designed* to save people from all sort of financial constraints and deficiencies through reading of *local and international news on our platform*. *We then share the revenue generated* to our affiliate members every week. </p>

                    <p>ZIP is: 

                    	<ul>
                    		<li>A platform where you can be earning  money by reading our online news. (Make Money)</li>
                    		<li>A program that allows you to work from the comfort of your home. (Fulfilment)</li>
                    		<li>A platform where you get sophisticated information about events, people, places and many more. (News)</li>
                    		<li>A Platform where you can make your business, product or company known to the world. (Advertisement)</li>
                    	</ul>
                    </p>

                    <br>
                    <p><b>For more info, Click on www.zeropoverty.ng <br>
                    Phone:07065642817,08130213125</b></p>
                </div>
            </div>
        </div>
    </div>
</div>
@include('footer-main')
@endsection
