@extends('layouts.app-dashboard')

@section('content')
		@section('css')
    		<link rel="stylesheet" href="{{ asset('css/classic.css') }}">
    		<link rel="stylesheet" href="{{ asset('css/classic.date.css') }}">

   		 @stop
<!-- ============ Body content start ============= -->
        <div class="main-content-wrap sidenav-open d-flex flex-column">
            <div class="breadcrumb">
                <h1>Zero Poverty</h1>
                <ul>
                    <li><a href="{{url('users/dashboard')}}">Dashboard</a></li>
                    <li>Edit Profile</li>
                </ul>
            </div>

            <div class="separator-breadcrumb border-top"></div>
			<div class="row">
                <div class="col-md-12">
                    <h4>Edit Profile</h4>
                    <div class="card mb-5">
                        <div class="card-body">
                            <form method="POST" action="{{ url('users/profile') }}" aria-label="Update Password">
                            	@csrf
                                <div class="row">
                                	<div class="col-md-6 col-sm-8">
	                                	<div class="form-group row">
		                                    <label for="name" class="col-sm-2 col-form-label">{{ __('Name *') }}</label>
		                                    <div class="col-sm-10 col-md-6">
					                            <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{$user->name}}" required>

					                            @if ($errors->has('name'))
					                                <span class="invalid-feedback" role="alert">
					                                	<strong>{{ $errors->first('name') }}</strong>
					                                </span>
					                            @endif
					                        </div>
		                                </div>
	                                </div>
	                                <div class="col-md-6 col-sm-8">
	                                	<div class="form-group row">
				                            <label for="mobile_number" class="col-sm-2 col-form-label">{{ __('Mobile Number') }}</label>

				                            <div class="col-sm-10 col-md-6">
				                                <input id="mobile_number" type="text" class="form-control{{ $errors->has('mobile_number') ? ' is-invalid' : '' }}" value="{{ $user->mobile_number }}" name="mobile_number">

				                                @if ($errors->has('mobile_number'))
				                                    <span class="invalid-feedback" role="alert">
				                                        <strong>{{ $errors->first('mobile_number') }}</strong>
				                                    </span>
				                                @endif
				                            </div>
				                        </div>
	                                </div>
                                </div>
                                <div class="row">
                                	<div class="col-md-6 col-sm-8">
	                                	<div class="form-group row">
				                            <label for="email" class="col-sm-2 col-form-label">{{ __('Email Address *') }}</label>

				                            <div class="col-sm-10 col-md-6">
				                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{$user->email}}
				                                "required>

				                                @if ($errors->has('email'))
				                                    <span class="invalid-feedback" role="alert">
				                                        <strong>{{ $errors->first('email') }}</strong>
				                                    </span>
				                                @endif
				                            </div>
				                        </div>
	                                </div>
	                                <div class="col-md-6 col-sm-8">
	                                	<div class="form-group row">
	                                        <label for="picker3" class="col-sm-2 col-form-label">Birth date</label>
	                                        <div class="col-sm-10 col-md-6">
	                                        	<input type="date" class="form-control" placeholder="yyyy-mm-dd" value="{{ $user->date_of_birth }}" name="date_of_birth">
	                                        </div>
	                                    </div>
	                                </div>
                                </div>
                                <div class="row">
                                	<div class="col-md-6 col-sm-8">
	                                	<div class="form-group row">
				                            <label for="phone_number" class="col-sm-2 col-form-label">{{ __('Phone Number') }}</label>

				                            <div class="col-sm-10 col-md-6">
				                                <input id="phone_number" type="text" class="form-control{{ $errors->has('phone_number') ? ' is-invalid' : '' }}" value="{{ $user->phone_number }}" name="phone_number">

				                                @if ($errors->has('phone_number'))
				                                    <span class="invalid-feedback" role="alert">
				                                        <strong>{{ $errors->first('phone_number') }}</strong>
				                                    </span>
				                                @endif
				                            </div>
				                        </div>
	                                </div>
	                                <div class="col-md-6 col-sm-8">
	                                	<div class="form-group row">
				                            <label for="address" class="col-sm-2 col-form-label">{{ __('Address') }}</label>

				                            <div class="col-sm-10 col-md-6">
				                                <textarea id="address" class="form-control" cols="7" rows="3" name="address"> {{ $user->address }}</textarea>

				                                @if ($errors->has('address'))
				                                    <span class="invalid-feedback" role="alert">
				                                        <strong>{{ $errors->first('address') }}</strong>
				                                    </span>
				                                @endif
				                            </div>
				                        </div>
	                                </div>
                                </div>
                                <div class="row">
                                	<div class="col-md-6 col-sm-8">
                                		<div class="form-group row">
				                            <label for="gender" class="col-sm-2 col-form-label">{{ __('Select Gender') }}</label>

				                            <div class="col-sm-10 col-md-6">
				                            	<span> Your Gender: {{ $user->gender }} </span>
				                                <select class="form-control" name="gender" id="gender">
				                                	<option value="Male">Male</option>
				                                	<option value="Female">Female</option>
				                                	<option value="Other">Other</option>
				                                </select>

				                                @if ($errors->has('gender'))
				                                    <span class="invalid-feedback" role="alert">
				                                        <strong>{{ $errors->first('gender') }}</strong>
				                                    </span>
				                                @endif
				                            </div>
				                        </div>
                                	</div>
                                	<div class="col-md-6 col-sm-8">
                                		<div class="form-group row">
				                            <label for="city" class="col-sm-2 col-form-label">{{ __('City') }}</label>

				                            <div class="col-sm-10 col-md-6">
				                                <input id="city" type="text" class="form-control{{ $errors->has('city') ? ' is-invalid' : '' }}" value="{{ $user->city }}" name="city">

				                                @if ($errors->has('city'))
				                                    <span class="invalid-feedback" role="alert">
				                                        <strong>{{ $errors->first('city') }}</strong>
				                                    </span>
				                                @endif
				                            </div>
				                        </div>
                                	</div>
                                </div>
                                <div class="row">
                                	<div class="col-md-6 col-sm-8">
                                		<div class="form-group row">
				                            <label for="facebook_username" class="col-sm-2 col-form-label">{{ __('Facebook Username or Url') }}</label>

				                            <div class="col-sm-10 col-md-6">
				                                <textarea id="facebook_username" class="form-control" cols="7" rows="3" name="facebook_username"> {{ $user->facebook_username }}</textarea>

				                                @if ($errors->has('facebook_username'))
				                                    <span class="invalid-feedback" role="alert">
				                                        <strong>{{ $errors->first('facebook_username') }}</strong>
				                                    </span>
				                                @endif
				                            </div>
				                        </div>
                                	</div>
                                	<div class="col-md-6 col-sm-8">
                                		<div class="form-group row">
				                            <label for="twitter_username" class="col-sm-2 col-form-label">{{ __('Twitter Handle') }}</label>

				                            <div class="col-sm-10 col-md-6">
				                                <textarea id="twitter_username" class="form-control" cols="7" rows="3" name="twitter_username"> {{ $user->twitter_username }}</textarea>

				                                @if ($errors->has('twitter_username'))
				                                    <span class="invalid-feedback" role="alert">
				                                        <strong>{{ $errors->first('twitter_username') }}</strong>
				                                    </span>
				                                @endif
				                            </div>
				                        </div>
                                	</div>
                                </div>
                                <div class="row">
                                	<div class="col-md-6 col-sm-8">
                                		<div class="form-group row">
				                            <label for="google_username" class="col-sm-2 col-form-label">{{ __('Google Handle') }}</label>

				                            <div class="col-sm-10 col-md-6">
				                                <textarea id="google_username" class="form-control" cols="7" rows="3" name="google_username"> {{ $user->google_username }}</textarea>

				                                @if ($errors->has('google_username'))
				                                    <span class="invalid-feedback" role="alert">
				                                        <strong>{{ $errors->first('google_username') }}</strong>
				                                    </span>
				                                @endif
				                            </div>
				                        </div>
                                	</div>
                                	<div class="col-md-6 col-sm-8">
                                		<div class="form-group row">
				                            <label for="about_me" class="col-sm-2 col-form-label">{{ __('About Me') }}</label>

				                            <div class="col-sm-10 col-md-6">
				                                <textarea id="about_me" class="form-control" cols="7" rows="3" name="about_me"> {{ $user->about_me }}</textarea>

				                                @if ($errors->has('about_me'))
				                                    <span class="invalid-feedback" role="alert">
				                                        <strong>{{ $errors->first('about_me') }}</strong>
				                                    </span>
				                                @endif
				                            </div>
				                        </div>
                                	</div>
                                </div>

                                <div class="row">
                                	<div class="col-md-6 col-sm-8">
                                		<div class="form-group row">

				                            <label for="image" class="col-sm-2 col-form-label text-md-right">{{ __('Image') }}</label>

				                            <div class="col-md-6 col-sm-10">
				                                <input id="image" type="file" class="form-control" name="image" >
				                            </div>
                                		</div>
                                	</div>
                                	<div class="col-md-6 col-sm-8">
                                		<div class="form-group row">
				                            <label for="signature" class="col-sm-2 col-form-label">{{ __('Signature') }}</label>

				                            <div class="col-sm-10 col-md-6">
				                                <textarea id="signature" class="form-control" cols="7" rows="3" name="signature"> {{ $user->signature }}</textarea>

				                                @if ($errors->has('signature'))
				                                    <span class="invalid-feedback" role="alert">
				                                        <strong>{{ $errors->first('signature') }}</strong>
				                                    </span>
				                                @endif
				                            </div>
				                        </div>
                                	</div>
                                </div>
                               

                                <div class="form-group row">
                                    <div class="col-sm-10">
                                        <button type="submit" class="btn btn-primary">{{ __('Update Profile') }}</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            @include('footer')
        </div>
        <!-- ============ Body content End ============= -->

    	@section('js')
    		<script src="{{ asset('js/picker.js') }}"></script>
    		<script src="{{ asset('js/picker.date.js') }}"></script>
		    <script type="text/javascript">
		        $(document).ready(function(){$("#picker3").pickadate()});
		    </script>

   		@stop
@endsection