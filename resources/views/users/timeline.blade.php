@extends('layouts.app-dashboard')

@section('content')

<!-- ============ Body content start ============= -->
        <div class="main-content-wrap sidenav-open d-flex flex-column">
            <div class="breadcrumb">
                <h1>Zero Poverty</h1>
                <ul>
                    <li><a href="{{url('users/dashboard')}}">Dashboard</a></li>
                    <li>User Profile</li>
                </ul>
            </div>

            <div class="separator-breadcrumb border-top"></div>

            <div class="row mb-4">
                <div class="col-md-12">
                    <a href="{{url('users/profile')}}" class="btn btn-primary text-white">{{ __('Edit Profile') }}</a>
                    <a href="{{url('users/bankInfo')}}" class="btn btn-primary text-white">{{ __('Update Bank Info') }}</a>
                </div>
            </div>
            <!-- end of row -->

            <div class="card user-profile o-hidden mb-4">
                <div class="header-cover" style="background-image: url('../img/photo-wide-4.jpg')"></div>
                <div class="user-info">
                    @if(Auth::user()->image == null)
                        <img class="profile-picture avatar-lg mb-2" src="{{asset('img/profile_avatar.jpeg')}}" alt="">
                    @else
                        <img class="profile-picture avatar-lg mb-2" src="{{ Auth::user()->image }}">
                    @endif

                    <p class="m-0 text-24">{{ Auth::user()->name }}</p>
                    <p class="text-muted m-0">{{ Auth::user()->email }}</p>
                    <p class="text-muted m-0">{{ Auth::user()->package_type }}</p>
                </div>
                <div class="card-body">
                    <ul class="nav nav-tabs profile-nav mb-4" id="profileTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="timeline-tab" data-toggle="tab" href="#timeline" role="tab" aria-controls="timeline" aria-selected="false">Timeline</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="profileTabContent">
                    	<div class="tab-pane fade active show" id="timeline" role="tabpanel" aria-labelledby="timeline-tab">
                            <h4>Personal Information</h4>
                            <p> {{ Auth::user()->about_me }}</p>
                            <hr>
                            <div class="row">
                                <div class="col-md-4 col-6">
                                    <div class="mb-4">
                                        <p class="text-primary mb-1"><i class="i-Calendar text-16 mr-1"></i> Birth Date</p>
                                        <span>{{ Auth::user()->date_of_birth }}</span>
                                    </div>
                                    <div class="mb-4">
                                        <p class="text-primary mb-1"><i class="i-Globe text-16 mr-1"></i> Address</p>
                                        <span>{{ Auth::user()->address }}</span>
                                    </div>
                                </div>
                                <div class="col-md-4 col-6">
                                    <div class="mb-4">
                                        <p class="text-primary mb-1"><i class="i-MaleFemale text-16 mr-1"></i> Gender</p>
                                        <span>{{ Auth::user()->gender }}</span>
                                    </div>
                                    <div class="mb-4">
                                        <p class="text-primary mb-1"><i class="i-MaleFemale text-16 mr-1"></i> Email</p>
                                        <span>{{ Auth::user()->email }}</span>
                                    </div>
                                </div>
                                <div class="col-md-4 col-6">
                                    <div class="mb-4">
                                        <p class="text-primary mb-1"><i class="i-Face-Style-4 text-16 mr-1"></i> Phone Number</p>
                                        <span>{{ Auth::user()->phone_number }}</span>
                                    </div>
                                    <div class="mb-4">
                                        <p class="text-primary mb-1"><i class="i-Professor text-16 mr-1"></i> Mobile Number</p>
                                        <span>{{ Auth::user()->mobile_number }}</span>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <h4>Bank Info</h4>
                            <p class="mb-4"></p>
                            <div class="row">
                                <div class="col-md-4 col-sm-8 col-6 text-center">
                                    <h4 class="text-16 mt-1">
                                        @if($bankInfo != null)
                                            @if(!empty($bankInfo->bank_name))
                                            {{ $user->bankInfo->bank_name }}</h4>
                                            @endif
                                        @endif
                                   <span><small>
                                        @if(!empty($bankInfo->account_type))
                                            {{ $user->bankInfo->account_type }}
                                        @endif
                                    </small></span>
                                </div>
                                 <div class="col-md-4 col-sm-8 col-6 text-center">
                                    <p class="text-16 mt-1">
                                        @if(!empty($bankInfo->account_name))
                                            {{ $user->bankInfo->account_name }}
                                        @endif
                                    </p>
                                </div>
                                <div class="col-md-4 col-sm-8 col-6 text-center">
                                    <p class="text-16 mt-1">
                                        @if(!empty($bankInfo->account_number))
                                            {{ $user->bankInfo->account_number }}
                                        @endif
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @include('footer')
        </div>
        <!-- ============ Body content End ============= -->

@endsection