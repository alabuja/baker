@extends('layouts.app-dashboard')

@section('content')
        @section('css')
            <link rel="stylesheet" href="{{ asset('css/sweetalert2.min.css') }}">
            <style type="text/css">
                    .tooltip {  position: relative; display: inline-block;  }
                    .tooltip .tooltiptext { visibility: hidden; width: 140px;
                      background-color: #555; color: #fff; text-align: center; border-radius: 6px; padding: 5px; position: absolute; z-index: 1; bottom: 150%;
                      left: 50%; margin-left: -75px; opacity: 0; transition: opacity 0.3s;
                    }
                    .tooltip .tooltiptext::after { content: ""; position: absolute; top: 100%; left: 50%; margin-left: -5px; border-width: 5px; border-style: solid; border-color: #555 transparent transparent transparent; }
                    .tooltip:hover .tooltiptext { visibility: visible; opacity: 1; }
            </style>
         @stop
<!-- ============ Body content start ============= -->
        <div class="main-content-wrap sidenav-open d-flex flex-column">
            <div class="breadcrumb">
                <h1>Zero Poverty</h1>
                <ul>
                    <li><a href="{{url('users/dashboard')}}">Dashboard</a></li>
                    <li>Home</li>
                </ul>
            </div>

            <div class="separator-breadcrumb border-top"></div>

            <div class="row">
                <div class="col-md-12">
                    <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4">
                        <div class="card-body">
                            <b style="font-size: 1.19em; margin-right: 8px;">Referral Link: </b><input type="text" id="referral_link" class="form-control col-md-7" value="{{url('/referral-register/'.Auth::user()->slug.'/'.Auth::user()->id) }}" readonly="readonly"> 
                                <button type="button" class="btn btn-primary" onclick="referralLink()" onmouseout="outFunc()" style="margin-left: 20px;">
                                        <span class="tooltiptext" id="myTooltip">Copy to clipboard</span>
                                </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4">
                        <div class="card-body text-center">
                            <div class="content">
                                <p class="text-muted mt-2 mb-0">Posts</p>
                                <p class="text-primary text-24 line-height-1 mb-2">  {{ \App\Models\Post::countPosts() }}</p></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4">
                        <div class="card-body text-center">
                            <div class="content">
                                <p class="text-muted mt-2 mb-0">Users</p>
                                <p class="text-primary text-24 line-height-1 mb-2">{{ \App\Models\User::countUsers() }}</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4">
                        <div class="card-body text-center">
                            <div class="content">
                                <p class="text-muted mt-2 mb-0">Sponsored Posts</p>
                                <p class="text-primary text-24 line-height-1 mb-2">{{ \App\Models\SponsoredPost::countPosts() }}</p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="row">
                <!-- ICON BG -->
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4">
                        <div class="card-body text-center">
                            <i class="i-Add-User"></i>
                            <div class="content">
                                <p class="text-muted mt-2 mb-0">Activity Earnings</p>
                                <p class="text-primary text-24 line-height-1 mb-2">         &#8358;{{ $currentEarnings }}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4">
                        <div class="card-body text-center">
                            <i class="i-Financial"></i>
                            <div class="content">
                                <p class="text-muted mt-2 mb-0">Referrals Earning</p>
                                <p class="text-primary text-24 line-height-1 mb-2">       &#8358;{{ $referrals }}</p>
                            </div>
                        </div>
                    </div>
                </div>
<!-- 
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4">
                        <div class="card-body text-center">
                            <i class="i-Checkout-Basket"></i>
                            <div class="content">
                                <p class="text-muted mt-2 mb-0">Last Time Withdraw</p>
                                <p class="text-primary text-24 line-height-1 mb-2">80</p>
                            </div>
                        </div>
                    </div>
                </div> -->

                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4">
                        <div class="card-body text-center">
                            <i class="i-Money-2"></i>
                            <div class="content">
                                <p class="text-muted mt-2 mb-0">All Time Earnings</p>
                                <p class="text-primary text-24 line-height-1 mb-2">        &#8358;{{ $allTimeEarnings }}</p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            @include('footer')
        </div>
        <!-- ============ Body content End ============= -->
<?php 
    
    $package = 0;
    if(Auth::user()->package_type == 'Free')
    {
        $package = true;
    }
?>
        @section('js')
            <script src="{{ asset('js/sweetalert2.min.js') }}"></script>

            <script type="text/javascript">
                function referralLink(){
                    let copyText = document.getElementById("referral_link");

                    copyText.select();
                    document.execCommand("copy");
  
                    let tooltip = document.getElementById("myTooltip");
                    tooltip.innerHTML = "Copied: " + copyText.value;

                }

                function outFunc() {
                    let tooltip = document.getElementById("myTooltip");
                    tooltip.innerHTML = "Copy to clipboard";
                }
                
                $(document).ready(function(){
                    let package =  {{ $package }};

                    if(package === 1){
                        swal({type:"warning",title:"Warning!",text:"Upgrade Your Account to enjoy the best of Zero Poverty!",buttonsStyling:!1,confirmButtonClass:"btn btn-lg btn-warning"})
                    }
                      
                });

            </script>

        @stop
@endsection