@extends('layouts.app-dashboard')

@section('content')
<!-- ============ Body content start ============= -->
        <div class="main-content-wrap sidenav-open d-flex flex-column">
            <div class="breadcrumb">
                <h1>Zero Poverty</h1>
                <ul>
                    <li><a href="{{ url('users/dashboard') }}">Dashboard</a></li>
                    <li>Edit Bank Info</li>
                </ul>
            </div>

            <div class="separator-breadcrumb border-top"></div>
			<div class="row">
                <div class="col-md-12">

                	@include("alerts")
                    <h4>Edit Profile</h4>
                    <div class="card mb-5">
                        <div class="card-body">
                            <form method="POST" action="{{ url('users/bankInfo') }}" aria-label="Update Bank Info">
                            	@csrf
                                <div class="row">
                                	<div class="col-md-6 col-sm-8">
	                                	<div class="form-group row">
		                                    <label for="account_name" class="col-sm-2 col-form-label">{{ __('Account Name') }}</label>
		                                    <div class="col-sm-10 col-md-6">
					                            @if(!empty($bankInfo->account_name))
					                            	<input id="account_name" type="text" class="form-control{{ $errors->has('account_name') ? ' is-invalid' : '' }}" name="account_name" value="{{ $user->bankInfo->account_name }}" required>
					                            @else
					                            	<input id="account_name" type="text" class="form-control{{ $errors->has('account_name') ? ' is-invalid' : '' }}" name="account_name" required>
					                            @endif

					                            @if ($errors->has('account_name'))
					                                <span class="invalid-feedback" role="alert">
					                                	<strong>{{ $errors->first('account_name') }}</strong>
					                                </span>
					                            @endif
					                        </div>
		                                </div>
	                                </div>
	                                <div class="col-md-6 col-sm-8">
	                                	<div class="form-group row">
				                            <label for="account_number" class="col-sm-2 col-form-label">{{ __('Account Number') }}</label>

				                            <div class="col-sm-10 col-md-6">
				                            	@if(!empty($bankInfo->account_number))
					                            	<input id="account_number" type="text" class="form-control{{ $errors->has('account_number') ? ' is-invalid' : '' }}" value="{{ $user->bankInfo->account_number }}" name="account_number" required>
					                            @else
					                            	<input id="account_number" type="text" class="form-control{{ $errors->has('account_number') ? ' is-invalid' : '' }}" name="account_number" required>
					                            @endif
				                                
				                                @if ($errors->has('account_number'))
				                                    <span class="invalid-feedback" role="alert">
				                                        <strong>{{ $errors->first('account_number') }}</strong>
				                                    </span>
				                                @endif
				                            </div>
				                        </div>
	                                </div>
                                </div>
                                <div class="row">
                                	<div class="col-md-6 col-sm-8">
                                		<div class="form-group row">
				                            <label for="bank_name" class="col-sm-2 col-form-label">{{ __('Select Bank Name') }}</label>

				                            <div class="col-sm-10 col-md-6">
				                            	<span> 
				                            		@if(!empty($bankInfo->bank_name))
				                            			Current Bank [{{ $user->bankInfo->bank_name }}]
				                            		@endif 
				                            	</span>
				                                <select class="form-control" name="bank_name" id="bank_name" required>
				                                	<option value="Access Bank">Access Bank</option>
				                                	<option value="ALAT by WEMA">ALAT by WEMA</option>
				                                	<option value="ASO Savings and Loans">ASO Savings and Loans</option>
				                                	<option value="Citibank Nigeria">Citibank Nigeria</option>
				                                	<option value="Diamond Bank">Diamond Bank</option>
				                                	<option value="Ecobank Nigeria">Ecobank Nigeria</option>
				                                	<option value="Ekondo Microfinance Bank">Ekondo Microfinance Bank</option>
				                                	<option value="Enterprise Bank">Enterprise Bank</option>
				                                	<option value="Fidelity Bank">Fidelity Bank</option>
				                                	<option value="First Bank of Nigeria">First Bank of Nigeria</option>
				                                	<option value="First City Monument Bank">First City Monument Bank</option>
				                                	<option value="Guaranty Trust Bank">Guaranty Trust Bank</option>
				                                	<option value="Heritage Bank">Heritage Bank</option>
				                                	<option value="Jaiz Bank">Jaiz Bank</option>
				                                	<option value="Keystone Bank">Keystone Bank</option>
				                                	<option value="MainStreet Bank">MainStreet Bank</option>
				                                	<option value="Parallex Bank">Parallex Bank</option>
				                                	<option value="Polaris Bank">Polaris Bank</option>
				                                	<option value="Providus Bank">Providus Bank</option>
				                                	<option value="Stanbic IBTC Bank">Stanbic IBTC Bank</option>
				                                	<option value="Standard Chartered Bank">Standard Chartered Bank</option>
				                                	<option value="Sterling Bank">Sterling Bank</option>
				                                	<option value="Suntrust Bank">Suntrust Bank</option>
				                                	<option value="Union Bank of Nigeria">Union Bank of Nigeria</option>
				                                	<option value="United Bank For Africa">United Bank For Africa</option>
				                                	<option value="Unity Bank">Unity Bank</option>
				                                	<option value="Wema Bank">Wema Bank</option>
				                                	<option value="Zenith Bank">Zenith Bank</option>
				                                </select>

				                                @if ($errors->has('bank_name'))
				                                    <span class="invalid-feedback" role="alert">
				                                        <strong>{{ $errors->first('bank_name') }}</strong>
				                                    </span>
				                                @endif
				                            </div>
				                        </div>
                                	</div>
                                	<div class="col-md-6 col-sm-8">
                                		<div class="form-group row">
				                            <label for="account_type" class="col-sm-2 col-form-label">{{ __('Select Account Type') }}</label>

				                            <div class="col-sm-10 col-md-6">
				                            	<span> 
				                            		@if(!empty($bankInfo->account_type))
				                            			Current Account Type [{{ $user->bankInfo->account_type }}]
				                            		@endif
				                            	</span>
				                                <select class="form-control" name="account_type" id="account_type" required>
				                                	<option value="Savings Account">Savings Account</option>
				                                	<option value="Platinum Premium Account">Platinum Premium Account</option>
				                                	<option value="Current Account">Current Account</option>
				                                	<option value="Gold Premium Account">Gold Premium Account</option>
				                                </select>

				                                @if ($errors->has('account_type'))
				                                    <span class="invalid-feedback" role="alert">
				                                        <strong>{{ $errors->first('account_type') }}</strong>
				                                    </span>
				                                @endif
				                            </div>
				                        </div>
                                	</div>
                                </div>
                               
                                <div class="form-group row">
                                    <div class="col-sm-10">
                                        <button type="submit" class="btn btn-primary">{{ __('Change Bank Details') }}</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            @include('footer')
        </div>
        <!-- ============ Body content End ============= -->
@endsection