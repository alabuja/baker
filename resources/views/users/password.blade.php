@extends('layouts.app-dashboard')

@section('content')
<!-- ============ Body content start ============= -->
        <div class="main-content-wrap sidenav-open d-flex flex-column">
            <div class="breadcrumb">
                <h1>Zero Poverty</h1>
                <ul>
                    <li><a href="{{url('users/dashboard')}}">Dashboard</a></li>
                    <li>Change Password</li>
                </ul>
            </div>

            <div class="separator-breadcrumb border-top"></div>
			<div class="row">
                <div class="col-md-6">
                    <h4>Change Pasword</h4>
                    <div class="card mb-5">
                        <div class="card-body">
                            <form method="POST" action="{{ url('users/password') }}" aria-label="Update Password">
                            	@csrf
                                <div class="form-group row">
                                    <label for="old" class="col-sm-2 col-form-label">{{ __('Current Password') }}</label>
                                    <div class="col-sm-10 col-md-6">
			                            <input id="old" type="password" class="form-control{{ $errors->has('old') ? ' is-invalid' : '' }}" name="old" required>

			                            @if ($errors->has('old'))
			                                <span class="invalid-feedback" role="alert">
			                                	<strong>{{ $errors->first('old') }}</strong>
			                                </span>
			                            @endif
			                        </div>
                                </div>
                                <div class="form-group row">
		                            <label for="password" class="col-sm-2 col-form-label">{{ __('New Password') }}</label>

		                            <div class="col-sm-10 col-md-6">
		                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

		                                @if ($errors->has('password'))
		                                    <span class="invalid-feedback" role="alert">
		                                        <strong>{{ $errors->first('password') }}</strong>
		                                    </span>
		                                @endif
		                            </div>
		                        </div>

		                        <div class="form-group row">
		                            <label for="password_confirmation" class="col-sm-2 col-form-label">{{ __('Confirm Password') }}</label>

		                            <div class="col-sm-10 col-md-6">
		                                <input id="password_confirmation" type="password" class="form-control{{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}" name="password_confirmation" required>

		                                @if ($errors->has('password_confirmation'))
		                                    <span class="invalid-feedback" role="alert">
		                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
		                                    </span>
		                                @endif
		                            </div>
		                        </div>

                                <div class="form-group row">
                                    <div class="col-sm-10">
                                        <button type="submit" class="btn btn-primary">{{ __('Update Password') }}</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            @include('footer')
        </div>
        <!-- ============ Body content End ============= -->


@endsection