@extends('layouts.app-dashboard')

@section('content')
<!-- ============ Body content start ============= -->
        <div class="main-content-wrap sidenav-open d-flex flex-column">
            <div class="breadcrumb">
                <h1>Zero Poverty</h1>
                <ul>
                    <li><a href="{{ url('users/dashboard') }}">Dashboard</a></li>
                    <li>Upgrade Account</li>
                </ul>
            </div>

            <div class="separator-breadcrumb border-top"></div>
			<div class="row">
                <div class="col-md-12">
                	@include("alerts")
                    <h4>Upgrade Account</h4>
                    <p>Upgrade to Affiliate Membership</p>
                    <h2>&#8358; 1100</h2>
                    <div class="card mb-5">
                        <div class="card-body">
                            <form method="POST" action="{{ url('pay') }}" aria-label="Upgrade Account">
                            	@csrf
                                <div class="row">
                                	<div class="col-md-8 col-offset-md-2 col-sm-8 col-offset-sm-2">
	                                	<div class="form-group row">
		                                    <div class="col-sm-10 col-md-6">
		                                    	<input type="hidden" name="email" value="{{ $user->email }}">
                            					<input type="hidden" name="metadata" value="{{ json_encode(['user_id' => $user->id]) }}">
					                            <input type="hidden" name="amount" value="110000">
                            					<input type="hidden" name="quantity" value="">
                            					<input type="hidden" name="reference" value="{{ Paystack::genTranxRef() }}">
                            					<input type="hidden" name="key" value="{{ config('paystack.publicKey') }}">
					                        </div>
		                                </div>
	                                </div>
                                </div>
                               
                                <div class="form-group row">
                                    <div class="col-sm-10">
                                        <button type="submit" class="btn btn-primary">{{ __('Upgrade Account') }}</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            @include('footer')
        </div>
        <!-- ============ Body content End ============= -->
@endsection