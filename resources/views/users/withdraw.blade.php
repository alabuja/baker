@extends('layouts.app-dashboard')

@section('content')
		@section('css')
    		<link rel="stylesheet" href="{{ asset('css/datatables.min.css') }}">

   		 @stop
	<!-- ============ Body content start ============= -->
        <div class="main-content-wrap sidenav-open d-flex flex-column">
            <div class="breadcrumb">
                <h1>Zero Poverty</h1>
                <ul>
                    <li><a href="{{ url('users/dashboard') }}">Dashboard</a></li>
                    <li>Withdrawal History</li>
                </ul>
            </div>

            <div class="separator-breadcrumb border-top"></div>
			<div class="row">
                <!-- ICON BG -->
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4">
                        <div class="card-body text-center">
                            <i class="i-Add-User"></i>
                            <div class="content">
                                <p class="text-muted mt-2 mb-0">Activity Earnings</p>
                                <p class="text-primary text-24 line-height-1 mb-2">         &#8358;{{ $currentAmount }}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4">
                        <div class="card-body text-center">
                            <i class="i-Financial"></i>
                            <div class="content">
                                <p class="text-muted mt-2 mb-0">Referrals Earning</p>
                                <p class="text-primary text-24 line-height-1 mb-2">       &#8358;{{ $referrals }}</p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="row">
            	<div class="col-md-12">
            		<div class="card row">
	            		@if($withDrawalStatus->status == 'Close' || $currentAmount < 700 && ($user->package_type == 'Free'))
	            			<h5 class="text-center" style="margin-top: 15px;"><b>Withdrawal Currently Closed</b></h5>
	            			<h5 class="text-center"><b>Minimium withdrawal Amount is &#8358; 2000 </b></h5>

	            			<div class="col-md-12" style="margin-bottom: 25px; margin-top: 25px;">
	            				<input type="text" class="form-control" value="Withdrawal Currently Closed" disabled>
	            			</div>

	            		@else
	            			<h5 class="text-center"><b>Withdrawal Currently Opened</b></h5>
	            			<div class="col-md-8">
	            				<a class="btn btn-primary float-center" href="#" data-toggle="modal" data-target="#withdrawal">Request Withdrawal</a>
	            			</div>
	            			
	            		@endif

	            	</div>
            	</div>
            </div>

            <div class="row" style="margin-top: 25px;">
                <div class="col-md-12">
                    <div class="card row">
                        <div class="card-body">
                            <h4 class="card-title text-center">Users Withdrawal History</h4>
                            <div class="table-responsive">
                                <table id="zero_configuration_table" class="display table table-striped table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>S/N</th>
                                            <th>Amount Requested</th>
                                            <th>Status</th>
                                            <th>Amount Paid</th>
                                            <th>Time</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($paymentRequests as $key => $paymentRequest)
                                        	<tr>
	                                            <td>{{ ++$key }}</td>
	                                            <td>&#8358; {{ $paymentRequest->amount_requested }}</td>
	                                            @if($paymentRequest->isPending == false && $paymentRequest->isApprove == false)
	                                            	<td style="background: #e5dd3b">Processing</td>
	                                            @elseif($paymentRequest->isApprove == true && $paymentRequest->isPending == true)
	                                            	<td style="background: #21ff00">Approved</td>
	                                            @elseif($paymentRequest->isApprove == false && $paymentRequest->isPending == true)
	                                            	<td style="background: #ff0000">Rejected</td>
	                                            @else
	                                            	<td>---</td>
	                                            @endif

	                                            @if($paymentRequest->amount_paid == '')
	                                            	<td> -- </td>
	                                            @else
	                                            	<td>&#8358; {{ $paymentRequest->amount_paid }}</td>
	                                            @endif
	                                            <td>{{ $paymentRequest->created_at->diffForHumans() }}</td>
	                                        </tr>
                                        @endforeach
                                        
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>S/N</th>
                                            <th>Amount Requested</th>
                                            <th>Status</th>
                                            <th>Amount Paid</th>
                                            <th>Time</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- end of col -->
            </div>

            <!-- Modal -->
			            <div class="modal fade" id="withdrawal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle-2" aria-hidden="true">
			                <div class="modal-dialog modal-dialog-centered" role="document">
			                    <div class="modal-content">
			                        <div class="modal-header">
			                            <h5 class="modal-title" id="exampleModalCenterTitle-2">Withdrawal</h5>
			                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			                                <span aria-hidden="true">&times;</span>
			                            </button>
			                        </div>
			                        <div class="modal-body">
			                            <form action="{{url('users/withdraw/paymentRequests')}}" method="POST">

				                        	@csrf
				            				<input type="text" name="amount_requested" value="{{ $currentAmount }}" class="form-control" readonly>
				            				<br>
				                        	<button class="btn btn-primary" type="submit">Submit</button>

			                        </div>
			                        <div class="modal-footer">
			                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			                        </div>
			                    </div>
			                </div>
			            </div>

            @include('footer')
        </div>
        <!-- ============ Body content End ============= -->

        @section('js')
    		<script src="{{ asset('js/datatables.min.js') }}"></script>
    		<script src="{{ asset('js/datatables.script.js') }}"></script>

   		@stop
@endsection