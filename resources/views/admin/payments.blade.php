@extends('layouts.app-admin')

@section('content')
		@section('admin-css')
    		<link rel="stylesheet" href="{{ asset('css/datatables.min.css') }}">

   		 @stop
<!-- ============ Body content start ============= -->
        <div class="main-content-wrap sidenav-open d-flex flex-column">
            <div class="breadcrumb">
                <h1>Zero Poverty</h1>
                <ul>
                    <li><a href="{{url('admin/dashboard')}}">Dashboard</a></li>
                    <li>Payments</li>
                </ul>
            </div>

            <div class="separator-breadcrumb border-top"></div>
                <div class="row mb-4">
                	<div class="col-md-12 mb-4">
                    <div class="card text-left">

                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="zero_configuration_table" class="display table table-striped table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>S/N</th>
	                                		<th>Name of User</th> 
	                                		<th>Amount</th>  
	                                		<th>Payment Reference</th>
	                                		<th>Payment Status</th>
	                                		<th>Date Paid</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($payments as $key => $payment)
                                        	<tr>
	                            				<td>{{++$key}}</td>
	                            				<td>{{$payment->user->name}}</td>
	                            				<td>&#8358 {{$payment->amount / 100}}</td>
	                                            <td>{{ $payment->payment_reference }}</td>
	                                            <td>
	                                            	@if($payment->has_paid == false)
	                                            	<span style="color: #ff0000;"> {{ __('UNPAID') }} </span>
	                                            	@else
	                                            	<span style="color: #1f890f;">{{ __('PAID') }}</span>
	                                            	@endif
	                                            </td>
	                                            <td>{{ $payment->created_at->toDateString() }}</td>
	                                        </tr>
                                        @endforeach
                                        
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>S/N</th>
	                                		<th>Name of User</th> 
	                                		<th>Amount</th>  
	                                		<th>Payment Reference</th>
	                                		<th>Payment Status</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>

                        </div>
                    </div>
                	</div>

                <!-- end of col -->
            	</div>

            @include('footer')
        </div>
        <!-- ============ Body content End ============= -->

   		 @section('admin-js')
    		<script src="{{ asset('js/datatables.min.js') }}"></script>
    		<script src="{{ asset('js/datatables.script.js') }}"></script>

   		@stop
@endsection