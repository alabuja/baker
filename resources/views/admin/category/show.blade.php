@extends('layouts.app-admin')

@section('content')
		@section('admin-css')
    		<link rel="stylesheet" href="{{ asset('css/datatables.min.css') }}">

   		 @stop
<!-- ============ Body content start ============= -->
        <div class="main-content-wrap sidenav-open d-flex flex-column">
            <div class="breadcrumb">
                <h1>Zero Poverty</h1>
                <ul>
                    <li><a href="{{url('admin/dashboard')}}">Dashboard</a></li>
                    <li>Categories</li>
                </ul>
            </div>

            <div class="row">
            	<div class="col-md-12">
            		<a class="btn btn-primary text-white float-right" data-toggle="modal" data-target="#category"> New Category</a>
            	</div>
            </div>

            <div class="separator-breadcrumb border-top"></div>
                <div class="row mb-4">
                	<div class="col-md-12 mb-4">
                    <div class="card text-left">

                		@include("alerts")

                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="zero_configuration_table" class="display table table-striped table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>S/N</th>
	                                		<th>Title</th> 
	                                		<th>Slug</th>  
	                                		<th>Date Created</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($categories as $key => $category)
                                        	<tr>
	                            				<td>{{++$key}}</td>
	                            				<td>{{$category->title}}</td>
	                            				<td>{{$category->slug}}</td>
	                                            <td>{{$category->created_at->toDateString() }}</td>
	                                        </tr>
                                        @endforeach
                                        
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>S/N</th>
	                                		<th>Title</th> 
	                                		<th>Slug</th>  
	                                		<th>Date Created</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>

                        </div>
                    </div>
                	</div>

                	<div class="modal fade" id="category" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle-2" aria-hidden="true">
			                <div class="modal-dialog modal-dialog-centered" role="document">
			                    <div class="modal-content">
			                        <div class="modal-header">
			                            <h5 class="modal-title" id="exampleModalCenterTitle-2">New Category</h5>
			                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			                                <span aria-hidden="true">&times;</span>
			                            </button>
			                        </div>
			                        <div class="modal-body">
			                            <form action="{{url('admin/category/new')}}" method="POST">
			                            	@csrf
			                            	<input type="text" name="title" class="form-control">
			                            	<br>
			                            	<button type="submit" class="btn btn-primary">Submit</button>
			                            </form>
			                        </div>
			                        <div class="modal-footer">
			                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			                        </div>
			                    </div>
			                </div>
			            </div>

                <!-- end of col -->
            	</div>

            @include('footer')
        </div>
        <!-- ============ Body content End ============= -->

   		 @section('admin-js')
    		<script src="{{ asset('js/datatables.min.js') }}"></script>
    		<script src="{{ asset('js/datatables.script.js') }}"></script>

   		@stop
@endsection