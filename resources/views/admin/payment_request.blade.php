@extends('layouts.app-admin')

@section('content')
		@section('admin-css')
    		<link rel="stylesheet" href="{{ asset('css/datatables.min.css') }}">

   		 @stop
<!-- ============ Body content start ============= -->
        <div class="main-content-wrap sidenav-open d-flex flex-column">
            <div class="breadcrumb">
                <h1>Zero Poverty</h1>
                <ul>
                    <li><a href="{{url('admin/dashboard')}}">Dashboard</a></li>
                    <li>Payment Requests</li>
                </ul>
            </div>

            <div class="separator-breadcrumb border-top"></div>
                <div class="row mb-4">
                	<div class="col-md-12 mb-4">
                    <div class="card text-left">

                		@include("alerts")

                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="zero_configuration_table" class="display table table-striped table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>S/N</th>
	                                		<th>Name of User</th> 
	                                		<th>Amount Requested</th>  
	                                		<th>Amount Paid</th>
	                                		<th>Approve</th>
	                                		<th>Referral Count</th>
	                                		<th>Reject</th>
	                                		<th>Date Requested</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($paymentRequests as $key => $paymentRequest)
                                        	<tr>
	                            				<td>{{++$key}}</td>
	                            				<td><a href="#" data-toggle="modal" data-target="#user{{$paymentRequest->user->id}}">{{$paymentRequest->user->name}}</a></td>
	                            				<td>&#8358 {{$paymentRequest->amount_requested}}
	                            					<br>

	                            					<p><strong style="color: #ff0000;">Referral Amount:</strong> {{ $paymentRequest->user->referral->where('has_paid', true)->where('package_type', 'Affiliate')->count() * 700 }}</p>
	                            					<p><strong style="color: #ff0000;">Remaining Amount: </strong> {{ $paymentRequest->amount_requested - ($paymentRequest->user->referral->where('has_paid', true)->where('package_type', 'Affiliate')->count() * 700) }}</p>
	                            				</td>
	                                            <td>&#8358 {{$paymentRequest->amount_paid }}</td>
	                                            <td><a style="text-decoration:none; cursor:pointer;" data-toggle="modal"
                                               data-target="#myModal{{$paymentRequest->id}}">Approve</a></td>
                                               <td>{{$paymentRequest->user->referral->where('has_paid', true)->where('package_type', 'Affiliate')->count()}}</td>
	                                            <td>
	                                            	<form action="{{url('admin/paymentRequests/reject/'.$paymentRequest->id)}}" method="POST">
	                            						@csrf
	                            					<button class="btn btn-danger" type="submit">Reject</button>
	                            				</form>
	                                            </td>
	                                            <td>{{ $paymentRequest->created_at->toDateString() }}</td>
	                                        </tr>
                                        @endforeach
                                        
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>S/N</th>
	                                		<th>Name of User</th> 
	                                		<th>Amount Requested</th>  
	                                		<th>Amount Paid</th>
	                                		<th>Approve</th>
	                                		<th>Referral Count</th>
	                                		<th>Reject</th>
	                                		<th>Date Requested</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>

                        </div>
                    </div>
                	</div>

                	@foreach($paymentRequests as $key => $paymentRequest)
	                	<!-- Modal -->
			            <div class="modal fade" id="myModal{{$paymentRequest->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle-2" aria-hidden="true">
			                <div class="modal-dialog modal-dialog-centered" role="document">
			                    <div class="modal-content">
			                        <div class="modal-header">
			                            <h5 class="modal-title" id="exampleModalCenterTitle-2">Amount Paid</h5>
			                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			                                <span aria-hidden="true">&times;</span>
			                            </button>
			                        </div>
			                        <div class="modal-body">
			                            <form action="{{url('admin/paymentRequests/approve/'.$paymentRequest->id)}}" method="POST">
			                            	@csrf
			                            	<input type="number" name="amount_paid" class="form-control">
			                            	<input type="hidden" name="referralCount" value="{{$paymentRequest->user->referral->count()}}">
			                            	<br>
			                            	<button type="submit" class="btn btn-primary">Submit</button>
			                            </form>
			                        </div>
			                        <div class="modal-footer">
			                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			                        </div>
			                    </div>
			                </div>
			            </div>
		            @endforeach


		            @foreach($paymentRequests as $key => $paymentRequest)
	                	<!-- Modal -->
			            <div class="modal fade" id="user{{$paymentRequest->user->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle-2" aria-hidden="true">
			                <div class="modal-dialog modal-dialog-centered" role="document">
			                    <div class="modal-content">
			                        <div class="modal-header">
			                            <h5 class="modal-title" id="exampleModalCenterTitle-2">User Profile</h5>
			                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			                                <span aria-hidden="true">&times;</span>
			                            </button>
			                        </div>
			                        <div class="modal-body">
			                        	<div class="row">
			                        		<div class="col-md-6">
			                        			<p>Name: <b>{{$paymentRequest->user->name}}</b></p>
			                        		</div>

			                        		<div class="col-md-6">
			                        			<p>Email: <b>{{$paymentRequest->user->email}}</b></p>
			                        		</div>
			                        	</div>

			                        	<div class="row">
			                        		<div class="col-md-6">
			                        			<p>Package Type: <b>{{$paymentRequest->user->package_type}}</b></p>
			                        		</div>

			                        		<div class="col-md-6">
			                        			<p>Facebook: <b>{{$paymentRequest->user->facebook_username}}</b></p>
			                        		</div>
			                        	</div>

			                        	<div class="row">
			                        		<div class="col-md-6">
			                        			<p>Phone Number: <b>{{$paymentRequest->user->phone_number}}</b></p>
			                        		</div>

			                        		<div class="col-md-6">
			                        			<p>Google: <b>{{$paymentRequest->user->google_username}}</b></p>
			                        		</div>
			                        	</div>

			                        	<div class="row">
			                        		<div class="col-md-6">
			                        			<p>Date Of Birth: <b>{{$paymentRequest->user->date_of_birth}}</b></p>
			                        		</div>

			                        		<div class="col-md-6">
			                        			<p>Twitter: <b>{{$paymentRequest->user->twitter_username}}</b></p>
			                        		</div>
			                        	</div>

			                        	<div class="row">
			                        		<div class="col-md-6">
			                        			<p>City: <b>{{$paymentRequest->user->city}}</b></p>
			                        		</div>

			                        		<div class="col-md-6">
			                        			<p>Address: <b>{{$paymentRequest->user->address}}</b></p>
			                        		</div>
			                        	</div>
			                        	<div class="row">
			                        		<div class="col-md-6">
			                        			<p>Bank Name: 
			                        				<b>
			                        					@if($paymentRequest->user != null && $paymentRequest->user->bankInfo != null)

			                        						{{ $paymentRequest->user->bankInfo->bank_name }}

			                        					@endif
			                        				</b>
			                        			</p>
			                        		</div>

			                        		<div class="col-md-6">
			                        			<p>Account Name: 
			                        				<b>
			                        					@if($paymentRequest->user != null && $paymentRequest->user->bankInfo != null)

			                        						{{ $paymentRequest->user->bankInfo->account_name }}
			                        					@endif
			                        				</b>
			                        			</p>
			                        		</div>
			                        	</div>
			                        	<div class="row">
			                        		<div class="col-md-6">
			                        			<p>Account Number: 
			                        				<b>
			                        					@if($paymentRequest->user != null && $paymentRequest->user->bankInfo != null)

			                        						{{ $paymentRequest->user->bankInfo->account_number }}
			                        					@endif
			                        				</b>
			                        			</p>
			                        		</div>

			                        		<div class="col-md-6">
			                        			<p>Account Type: 
			                        				<b>
			                        					@if($paymentRequest->user != null && $paymentRequest->user->bankInfo != null)

			                        						{{ $paymentRequest->user->bankInfo->account_type }}
			                        					@endif
			                        				</b>
			                        			</p>
			                        		</div>
			                        	</div>
			                        </div>
			                        <div class="modal-footer">
			                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			                        </div>
			                    </div>
			                </div>
			            </div>
		            @endforeach

                <!-- end of col -->
            	</div>

            @include('footer')
        </div>
        <!-- ============ Body content End ============= -->

   		 @section('admin-js')
    		<script src="{{ asset('js/datatables.min.js') }}"></script>
    		<script src="{{ asset('js/datatables.script.js') }}"></script>
    		<script src="{{ asset('js/modal.script.js') }}"></script>

   		@stop
@endsection