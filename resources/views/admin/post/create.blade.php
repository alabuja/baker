@extends('layouts.app-admin')

@section('content')

	@section('admin-css')
		<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
    @stop
<!-- ============ Body content start ============= -->
        <div class="main-content-wrap sidenav-open d-flex flex-column">
            <div class="breadcrumb">
                <h1>Zero Poverty</h1>
                <ul>
                    <li><a href="{{url('admin/dashboard')}}">Dashboard</a></li>
                    <li>Posts</li>
                </ul>
            </div>

            @include("alerts")
            
            <div class="separator-breadcrumb border-top"></div>
			<div class="row">
                <div class="col-md-12">
                    <h4>Create Posts</h4>
                    <div class="card">
                        <div class="card-body">
                            <form method="POST" action="{{ url('admin/posts/create') }}" aria-label="Create Posts">
                            	@csrf
                                <div class="form-group row">
                                    <label for="title" class="col-sm-2 col-md-2 col-form-label">{{ __('Title') }}</label>
                                    <div class="col-sm-10 col-md-6">
			                            <textarea rows="3" cols="7" id="title" class="form-control" name="title" required></textarea>
			                        </div>
                                </div>
                                <div class="form-group row">
		                            <label for="body" class="col-sm-2 col-md-2 col-form-label">{{ __('Body') }}</label>

		                            <div class="col-sm-10 col-md-8">
		                                <textarea id="summernote" name="body" required></textarea>
		                            </div>
		                        </div>

		                        <div class="form-group row">
		                            <label for="categories" class="col-sm-2 col-md-2 col-form-label">{{ __('Categories') }}</label>

		                            <div class="col-sm-10 col-md-6">
		                                <select class="form-control" name="category_id" id="categories" required>
		                                	@foreach($categories as $category)
		                                		<option value="{{$category->id}}">{{$category->title}}</option>
		                                	@endforeach
		                                </select>
		                            </div>
		                        </div>

		                        <div class="form-group row">
		                            <label for="post_type" class="col-sm-2 col-md-2 col-form-label">{{ __('Post Type') }}</label>

		                            <div class="col-sm-10 col-md-6">
		                                <select class="form-control" name="post_type" id="post_type" required>
		                                	<option value="pin">Pin</option>
		                                	<option value="earnable">Earnable</option>
		                                	<option value="admin">Admin</option>
		                                </select>
		                            </div>
		                        </div>

                                <div class="form-group row">
                                    <div class="col-sm-10">
                                        <button type="submit" class="btn btn-primary">{{ __('Create Posts') }}</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            @include('footer')
        </div>
        <!-- ============ Body content End ============= -->

@section('admin-js')
    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>     
 
    <script>
        $(document).ready(function() {
            $('#summernote').summernote({
                height: 250
            });
        });
    </script>

@stop

@endsection