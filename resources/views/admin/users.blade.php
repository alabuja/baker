@extends('layouts.app-admin')

@section('content')
		@section('admin-css')
    		<link rel="stylesheet" href="{{ asset('css/datatables.min.css') }}">

   		 @stop
<!-- ============ Body content start ============= -->
        <div class="main-content-wrap sidenav-open d-flex flex-column">
            <div class="breadcrumb">
                <h1>Zero Poverty</h1>
                <ul>
                    <li><a href="{{url('admin/dashboard')}}">Dashboard</a></li>
                    <li>Users List</li>
                </ul>
            </div>

            <div class="separator-breadcrumb border-top"></div>
                <div class="row mb-4">
                	<div class="col-md-12 mb-4">
                    <div class="card text-left">

                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="zero_configuration_table" class="display table table-striped table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>S/N</th>
	                                		<th>Name</th> 
	                                		<th>Email</th>  
	                                		<th>Facebook</th>
	                                		<th>Package Type</th>
	                                		<th>Date Registered</th>
	                                		<th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($users as $key => $user)
                                        	<tr>
	                            				<td>{{ ++$key }}</td>
	                            				<td>{{ $user->name }}</td>
	                            				<td>{{ $user->email }}</td>
	                                            <td>{{ $user->facebook_username }}</td>
	                                            <td>
	                                            	{{ $user->package_type }}
	                                            </td>
	                                            <td>{{ $user->created_at->toDateString() }}</td>
	                                            <td><a href="#" class="btn btn-primary" data-toggle="modal" data-target="#user{{$user->id}}">View</a></td>
	                                        </tr>
                                        @endforeach
                                        
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>S/N</th>
	                                		<th>Name</th> 
	                                		<th>Email</th>  
	                                		<th>Image</th>
	                                		<th>Package Type</th>
	                                		<th>Date Registered</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>

                        </div>
                    </div>
                	</div>

                	@foreach($users as $key => $user)
	                	<!-- Modal -->
			            <div class="modal fade" id="user{{$user->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle-2" aria-hidden="true">
			                <div class="modal-dialog modal-dialog-centered" role="document">
			                    <div class="modal-content">
			                        <div class="modal-header">
			                            <h5 class="modal-title" id="exampleModalCenterTitle-2">User Profile</h5>
			                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			                                <span aria-hidden="true">&times;</span>
			                            </button>
			                        </div>
			                        <div class="modal-body">
			                        	<div class="row">
			                        		<div class="col-md-6">
			                        			<p>Name: <b>{{$user->name}}</b></p>
			                        		</div>

			                        		<div class="col-md-6">
			                        			<p>Email: <b>{{$user->email}}</b></p>
			                        		</div>
			                        	</div>

			                        	<div class="row">
			                        		<div class="col-md-12">
			                        			<div class="col-md-6">
				                        			<p>Package Type: <b>{{$user->package_type}}</b></p>
				                        		</div>

				                        		<div class="col-md-6">
				                        			<p>Facebook: <b>{{$user->facebook_username}}</b></p>
				                        		</div>
			                        		</div>
			                        	</div>

			                        	<div class="row">
			                        		<div class="col-md-6">
			                        			<p>Phone Number: <b>{{$user->phone_number}}</b></p>
			                        		</div>

			                        		<div class="col-md-6">
			                        			<p>Google: <b>{{$user->google_username}}</b></p>
			                        		</div>
			                        	</div>

			                        	<div class="row">
			                        		<div class="col-md-6">
			                        			<p>Date Of Birth: <b>{{$user->date_of_birth}}</b></p>
			                        		</div>

			                        		<div class="col-md-6">
			                        			<p>Twitter: <b>{{$user->twitter_username}}</b></p>
			                        		</div>
			                        	</div>

			                        	<div class="row">
			                        		<div class="col-md-6">
			                        			<p>City: <b>{{$user->city}}</b></p>
			                        		</div>

			                        		<div class="col-md-6">
			                        			<p>Address: <b>{{$user->address}}</b></p>
			                        		</div>
			                        	</div>
			                        	<div class="row">
			                        		<div class="col-md-6">
			                        			<p>Bank Name: <b> @if($user->bankInfo != null)
			                        				{{ $user->bankInfo->bank_name }}
			                        			@endif</b></p>
			                        		</div>

			                        		<div class="col-md-6">
			                        			<p>Account Name: <b> 
			                        				@if($user->bankInfo != null)
			                        				{{ $user->bankInfo->account_name }}
			                        				@endif
			                        			</b></p>
			                        		</div>
			                        	</div>
			                        	<div class="row">
			                        		<div class="col-md-6">
			                        			<p>Account Number: <b>
			                        				@if($user->bankInfo != null)
			                        				{{ $user->bankInfo->account_number }}
			                        				@endif
			                        			</b></p>
			                        		</div>

			                        		<div class="col-md-6">
			                        			<p>Account Type: <b>
			                        				@if($user->bankInfo != null)
			                        				{{ $user->bankInfo->account_type }}
			                        				@endif
			                        			</b></p>
			                        		</div>
			                        	</div>
			                        </div>
			                        <div class="modal-footer">
			                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			                        </div>
			                    </div>
			                </div>
			            </div>
		            @endforeach

                <!-- end of col -->
            	</div>

            @include('footer')
        </div>
        <!-- ============ Body content End ============= -->

   		 @section('admin-js')
    		<script src="{{ asset('js/datatables.min.js') }}"></script>
    		<script src="{{ asset('js/datatables.script.js') }}"></script>

   		@stop
@endsection