@extends('layouts.app-admin')

@section('content')

<!-- ============ Body content start ============= -->
        <div class="main-content-wrap sidenav-open d-flex flex-column">
            <div class="breadcrumb">
                <h1>Zero Poverty</h1>
                <ul>
                    <li><a href="{{url('admin/dashboard')}}">Dashboard</a></li>
                    <li>Home</li>
                </ul>
            </div>

            <div class="separator-breadcrumb border-top"></div>
            <div class="row">
                <div class="col-lg-3 col-md-4">
                    <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4">
                        <div class="card-body text-center">
                            <div class="content">
                                <p class="text-muted mt-2 mb-0">Posts</p>
                                <p class="text-primary text-24 line-height-1 mb-2">  {{ \App\Models\Post::countPosts() }}</p></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4">
                    <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4">
                        <div class="card-body text-center">
                            <div class="content">
                                <p class="text-muted mt-2 mb-0">Users</p>
                                <p class="text-primary text-24 line-height-1 mb-2"><a href="{{url('admin/users')}}">{{ \App\Models\User::countUsers() }}</a></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4">
                    <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4">
                        <div class="card-body text-center">
                            <div class="content">
                                <p class="text-muted mt-2 mb-0">Sponsored Posts</p>
                                <p class="text-primary text-24 line-height-1 mb-2">{{ \App\Models\SponsoredPost::countPosts() }}</p>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Advertisement -->
                <div class="col-lg-3 col-md-4">
                	
                </div>
            </div>

            {{-- <div class="row">
                <!-- ICON BG -->
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4">
                        <div class="card-body text-center">
                            <i class="i-Add-User"></i>
                            <div class="content">
                                <p class="text-muted mt-2 mb-0">Activity Earnings</p>
                                <p class="text-primary text-24 line-height-1 mb-2">         &#8358; $currentEarnings
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4">
                        <div class="card-body text-center">
                            <i class="i-Financial"></i>
                            <div class="content">
                                <p class="text-muted mt-2 mb-0">Referrals Earning</p>
                                <p class="text-primary text-24 line-height-1 mb-2">       &#8358; $referrals</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4">
                        <div class="card-body text-center">
                            <i class="i-Checkout-Basket"></i>
                            <div class="content">
                                <p class="text-muted mt-2 mb-0">Last Time Withdraw</p>
                                <p class="text-primary text-24 line-height-1 mb-2">80</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4">
                        <div class="card-body text-center">
                            <i class="i-Money-2"></i>
                            <div class="content">
                                <p class="text-muted mt-2 mb-0">All Time Earnings</p>
                                <p class="text-primary text-24 line-height-1 mb-2">        &#8358;$allTimeEarnings </p>
                            </div>
                        </div>
                    </div>
                </div>

            </div> --}}

            @include('footer')
        </div>
        <!-- ============ Body content End ============= -->

@endsection