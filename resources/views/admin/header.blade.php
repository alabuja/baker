<div class="main-header">
    <div class="logo text-center">
        <!-- <img src="" alt=""> -->
        <a href="{{url('admin/dashboard')}}">
            Zero Poverty
        </a>
    </div>

    <div class="menu-toggle">
        <div></div>
        <div></div>
        <div></div>
    </div>

    <div class="d-flex align-items-center">
        <div class="search-bar">
            <input type="text" placeholder="Search">
            <i class="search-icon text-muted i-Magnifi-Glass1"></i>
        </div>
    </div>

    <div style="margin: auto"></div>

    <div class="header-part-right">
        <!-- Full screen toggle -->
        <i class="i-Full-Screen header-icon d-none d-sm-inline-block" data-fullscreen></i>

        <!-- User avatar dropdown -->
        <div class="dropdown">
            <div class="user col align-self-end">
                @if(Auth::guard('admin')->user()->image_url == null)
                    <img id="userDropdown" src="{{asset('img/profile_avatar.jpeg')}}" style="border-radius: 50%;" alt="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                @else
                    <img id="userDropdown" src="{{ Auth::guard('admin')->user()->image_url }}" style="border-radius: 50%;" alt="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                @endif

                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                    <div class="dropdown-header">
                        <i class="i-Lock-User mr-1"></i>{{ Auth::guard('admin')->user()->name }}
                    </div>
                    <a class="dropdown-item" href="{{ url('admin/profile') }}">Account settings</a>
                    <a class="dropdown-item" href="{{ url('admin/password') }}">Change Password</a>

                    <a class="dropdown-item" href=" {{ url('admin/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Sign out</a>

                        <form id="logout-form" action="{{ url('admin/logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                </div>
            </div>
        </div>
    </div>
</div>