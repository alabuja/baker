
        <div class="side-content-wrap">
            <div class="sidebar-left open" data-perfect-scrollbar data-suppress-scroll-x="true">
                <ul class="navigation-left">
                    <li class="nav-item active">
                        <a class="nav-item-hold" href="{{ url('admin/dashboard') }}">
                            <i class="nav-icon i-Bar-Chart"></i>
                            <span class="nav-text">Dashboard</span>
                        </a>
                        <div class="triangle"></div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-item-hold" href="{{ url('admin/payments') }}">
                            <i class="nav-icon i-Library"></i>
                            <span class="nav-text">Payments</span>
                        </a>
                        <div class="triangle"></div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-item-hold" href="{{ url('admin/set') }}">
                            <i class="nav-icon i-Library"></i>
                            <span class="nav-text">Set Withdrawal</span>
                        </a>
                        <div class="triangle"></div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-item-hold" href="{{ url('admin/users') }}">
                            <i class="nav-icon i-Library"></i>
                            <span class="nav-text">Users</span>
                        </a>
                        <div class="triangle"></div>
                    </li>
                    <li class="nav-item" data-item="affiliate">
                        <a class="nav-item-hold" href="#">
                            <i class="nav-icon i-Suitcase"></i>
                            <span class="nav-text">Posts</span>
                        </a>
                        <div class="triangle"></div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-item-hold" href="{{ url('admin/paymentRequests') }}">
                            <i class="nav-icon i-Computer-Secure"></i>
                            <span class="nav-text">Payment Requests</span>
                        </a>
                        <div class="triangle"></div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-item-hold" href="{{ url('admin/categories') }}">
                            <i class="nav-icon i-Administrator"></i>
                            <span class="nav-text">Categories</span>
                        </a>
                        <div class="triangle"></div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-item-hold" href="{{ url('admin/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            <i class="nav-icon i-Double-Tap"></i>
                            <span class="nav-text">Logout</span>
                        </a>

                        <form id="logout-form" action="{{ url('admin/logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>

                        <div class="triangle"></div>
                    </li>
                </ul>
            </div>

            <div class="sidebar-left-secondary" data-perfect-scrollbar data-suppress-scroll-x="true">
                <ul class="childNav" data-parent="affiliate">
                    <li class="nav-item">
                        <a href="{{ url('admin/sponsor/posts') }}">
                            <i class="nav-icon i-Crop-2"></i>
                            <span class="item-name">Sponsored Post</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ url('admin/posts') }}">
                            <i class="nav-icon i-Loading-3"></i>
                            <span class="item-name">Posts</span>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="sidebar-overlay"></div>
        </div>
        <!--=============== Left side End ================-->
