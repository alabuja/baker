@extends('layouts.app-admin')

@section('content')
		@section('admin-css')
    		<link rel="stylesheet" href="{{ asset('css/datatables.min.css') }}">

   		 @stop
<!-- ============ Body content start ============= -->
        <div class="main-content-wrap sidenav-open d-flex flex-column">
            <div class="breadcrumb">
                <h1>Zero Poverty</h1>
                <ul>
                    <li><a href="{{url('admin/dashboard')}}">Dashboard</a></li>
                    <li>Payments</li>
                </ul>
            </div>

            <div class="separator-breadcrumb border-top"></div>
                <div class="row mb-4">
                	<div class="col-md-12 mb-4">
                    <div class="card text-left">

                    	@include("alerts")

                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="zero_configuration_table" class="display table table-striped table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
	                                		<th>Current Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
	                                        <td>
	                                            @if($withdrawal->status == 'Close')
		                                            <form action="{{url('admin/set/open')}}" method="POST">
		                            					@csrf
		                            					<input type="hidden" name="status" value="Open">
		                            					<button class="btn btn-primary" type="submit">Open</button>
		                            				</form>
		                            			@else
	                                            	<form action="{{url('admin/set/close')}}" method="POST">
		                            					@csrf
		                            					<input type="hidden" name="status" value="Close">
		                            					<button class="btn btn-danger" type="submit">Close</button>
		                            				</form>
	                                            @endif
	                                        </td>
	                                    </tr>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>S/N</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>

                        </div>
                    </div>
                	</div>

                <!-- end of col -->
            	</div>

            @include('footer')
        </div>
        <!-- ============ Body content End ============= -->

   		 @section('admin-js')
    		<script src="{{ asset('js/datatables.min.js') }}"></script>
    		<script src="{{ asset('js/datatables.script.js') }}"></script>

   		@stop
@endsection