@extends('layouts.app-admin')

@section('content')
		@section('admin-css')
    		<link rel="stylesheet" href="{{ asset('css/classic.css') }}">
    		<link rel="stylesheet" href="{{ asset('css/classic.date.css') }}">

   		 @stop
<!-- ============ Body content start ============= -->
        <div class="main-content-wrap sidenav-open d-flex flex-column">
            <div class="breadcrumb">
                <h1>Zero Poverty</h1>
                <ul>
                    <li><a href="{{url('admin/dashboard')}}">Dashboard</a></li>
                    <li>Edit Profile</li>
                </ul>
            </div>

            <div class="separator-breadcrumb border-top"></div>
			<div class="row">
                <div class="col-md-12">
                    <h4>Edit Profile</h4>
                    <div class="card mb-5">
                        <div class="card-body">
                            <form method="POST" action="{{ url('admin/profile') }}" aria-label="Update Password" enctype="multipart/form-data">
                            	@csrf
                                <div class="row">
                                	<div class="col-md-6 col-sm-8">
	                                	<div class="form-group row">
		                                    <label for="name" class="col-sm-2 col-form-label">{{ __('Name *') }}</label>
		                                    <div class="col-sm-10 col-md-6">
					                            <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{Auth::guard('admin')->user()->name}}" required>

					                            @if ($errors->has('name'))
					                                <span class="invalid-feedback" role="alert">
					                                	<strong>{{ $errors->first('name') }}</strong>
					                                </span>
					                            @endif
					                        </div>
		                                </div>
	                                </div>
	                                <div class="col-md-6 col-sm-8">
	                                	<div class="form-group row">
				                            <label for="email" class="col-sm-2 col-form-label">{{ __('Email *') }}</label>

				                            <div class="col-sm-10 col-md-6">
				                                <input id="email" type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" value="{{ Auth::guard('admin')->user()->email }}" name="email">

				                                @if ($errors->has('email'))
				                                    <span class="invalid-feedback" role="alert">
				                                        <strong>{{ $errors->first('email') }}</strong>
				                                    </span>
				                                @endif
				                            </div>
				                        </div>
	                                </div>
                                </div>
                                <div class="row">
                                	<div class="col-md-6 col-sm-8">
	                                	<div class="form-group row">
				                            <label for="username" class="col-sm-2 col-form-label">{{ __('UserName') }}</label>

				                            <div class="col-sm-10 col-md-6">
				                                <input id="username" type="text" class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}" name="username" value="{{Auth::guard('admin')->user()->username}}
				                                " disabled>

				                                @if ($errors->has('username'))
				                                    <span class="invalid-feedback" role="alert">
				                                        <strong>{{ $errors->first('username') }}</strong>
				                                    </span>
				                                @endif
				                            </div>
				                        </div>
	                                </div>
	                                
                                	<div class="col-md-6 col-sm-8">
                                		<div class="form-group row">
                                			<label for="image" class="col-sm-2 col-form-label text-md-right">{{ __('Image') }}</label>

				                            <div class="col-md-6 col-sm-10">
				                                <input id="image" type="file" class="form-control{{ $errors->has('image') ? ' is-invalid' : '' }}" name="image"  required>

				                                @if ($errors->has('image'))
				                                    <span class="invalid-feedback" role="alert">
				                                        <strong>{{ $errors->first('image') }}</strong>
				                                    </span> 
				                                @endif
				                            </div>
                                		</div>

                                		@if(!empty(Auth::guard('admin')->user()->image_url))
		                                	<h4>You have an image</h4>
		                                @else
		                                	<h4>You have no image yet</h4>
		                                @endif
                                	</div>
                                </div>
                               

                                <div class="form-group row">
                                    <div class="col-sm-10">
                                        <button type="submit" class="btn btn-primary">{{ __('Update Profile') }}</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            @include('footer')
        </div>
        <!-- ============ Body content End ============= -->

    	@section('admin-js')
    		<script src="{{ asset('js/picker.js') }}"></script>
    		<script src="{{ asset('js/picker.date.js') }}"></script>
		    <script type="text/javascript">
		        $(document).ready(function(){$("#picker3").pickadate()});
		    </script>

   		@stop
@endsection