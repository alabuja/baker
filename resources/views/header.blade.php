<div class="main-header">
    <div class="logo text-center">
        <!-- <img src="" alt=""> -->
        <a href="{{url('/')}}">
            Zero Poverty
        </a>
    </div>

    <div class="menu-toggle">
        <div></div>
        <div></div>
        <div></div>
    </div>

    <div class="d-flex align-items-center">
        <div class="search-bar">
            <input type="text" placeholder="Search">
            <i class="search-icon text-muted i-Magnifi-Glass1"></i>
        </div>
    </div>

    <div style="margin: auto"></div>

    <div class="header-part-right">
        <!-- Full screen toggle -->
        <i class="i-Full-Screen header-icon d-none d-sm-inline-block" data-fullscreen></i>

        <!-- User avatar dropdown -->
        <div class="dropdown">
            <div class="user col align-self-end">
                @if(Auth::user()->image == null)
                    <img id="userDropdown" src="{{asset('img/profile_avatar.jpeg')}}" style="border-radius: 50%;" alt="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                @else
                    <img src="{{ Auth::user()->image }}" style="border-radius: 50%;">
                @endif

                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                    <div class="dropdown-header">
                        <i class="i-Lock-User mr-1"></i> {{ Auth::user()->name }}
                    </div>
                    <a class="dropdown-item" href="{{ url('users/profile') }}">Account settings</a>
                    <a class="dropdown-item" href="{{ url('users/password') }}">Change Password</a>

                    <a class="dropdown-item" href=" {{ url('users/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Sign out</a>

                        <form id="logout-form" action="{{ url('users/logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                </div>
            </div>
        </div>
    </div>
</div>