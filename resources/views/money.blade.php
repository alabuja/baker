@extends('layouts.app3')

@section('content')
@section('title')
  Make Money
@endsection
<div class="container" id="login">
    <div class="row">
        <div class="col-md-8">
            <div class="panel">

                <div class="panel-body">
                    <h3>How To Make Money on ZeroPoverty.ng</h3>
                    <br>
                    <p>Have you ever wondered how Bloggers in Nigeria make Millions of Naira every month?</p>

                    <p>It’s simple! Google and other adverts Companies pay Bloggers to place adverts on their blogs/websites and the bloggers get paid when you visit and click on those adverts!</p>

                    <p>This simply means bloggers make money when you visit their blog or website! So, the more you visit, the more money they make!</p>

                    <p>The question is, why do bloggers take all the money for themselves without giving a dime to the visitors who *helped* them make the money?</p>

                    <br>

                    <p>This is the unique difference we are making on www.zeropoverty.ng.</p>
                    <p>We have designed a system that allows us share our Revenue with our affiliate members every month, for performing activities in our Forum.</p>

                    <p>When you become an affiliate member, you can earn above #50,000 every  week and #200,000 every month respectively .</p>

                    <br>
                    <h5 style="color: #ff0000;">How genuine is zeropoverty.ng program ??</h5>

                    <p>You may be wondering, is this another scam? No! not at all.. Our goal is to reduce poverty to the minimal level.</p>

                    <p>Zeropoverty.ng  is a noble idea created to fight poverty by converting your time, data and activities online to a meaningful venture that *earns* you reasonable money.</p>

                    <p>We believe that since it costs money to be online (data,time,energy and phone/laptop), you should also be making Money while online!</p>

                    <br>
                    <h5 style="color: #ff0000;">How can I Earn Money on zeropoverty program :</h5>

                    <p>Members of zeropoverty platform  earn money in various ways depending on their levels of membership. </p>

                    <p>When you register a free account, you will not have earning ability until you upgrade your account to affiliate membership.</p>

					<p>Thus, For a member to earn money, he/she must be an affiliate member</p>

					<br>

					<h5><b>AFFILIATE MEMBERSHIP: </b></h5>
					<p>This is a basic plan. When you are an affiliate member, you will Earn:</p>
					<p>(A) N100 - Registration bonus. </p>
					<p>(B) N70 - Daily login bonus. </p>
					<p>(C) N50 - For each sponsored post you share on Facebook. </p>
					<p>(D) N4 - Comment bonus per post on Homepage. </p>
					<p>(E) N700 - Referral Bonus for each person you referred on zeropoverty platform  that becomes an affiliate</p>
                </div>
            </div>
        </div>
    </div>
</div>
@include('footer-main')
@endsection
