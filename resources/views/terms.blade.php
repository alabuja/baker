@extends('layouts.app3')

@section('content')
@section('title')
  Terms & Conditions
@endsection
<div class="container" id="login">
    <div class="row">
        <div class="col-md-8">
            <div class="panel">

                <div class="panel-body">
                    <h3>Terms of service</h3>
                    <p>Zeropoverty  Income Program (ZIP) Terms of Service.</p>

                    <h5>Introduction</h5>

                    <p>The following Terms and Conditions control your membership on Zeropoverty   Income Program . You agree that you have read and understand this Agreement (“Agreement”) and that your membership on Zeropoverty   Income Program (the “Program”) shall be subject to the following Terms and Conditions between you (the “Member”) and Zeropoverty  Income Program.</p>

                    <p>These Terms and Conditions may be modified at any time by Zeropoverty  Income Program Administrators without notice. Please review them from time to time since your ongoing use is subject to the terms and conditions as modified. Your continued participation in Zeropoverty Income Program after such modification shall be deemed to be your acceptance of any such modification. If you do not agree to these Terms and Conditions, please do not register to become a member of Zeropoverty  Income Program.</p>

                    <h5>Terms of Participation</h5>
                    <p>Member must be 18 years of age or older to participate. Members must provide Zeropoverty  Income Program with accurate, complete and updated registration information, including but not limited to an accurate name, mailing address and email address.</p>

                    <p>To the full extent allowed by applicable Nigerian law, Zeropoverty  Income Program at its sole discretion and for any or no reason may refuse to accept applications for membership.</p>
                    <p><b>Members may not:</b>
                    	<ol>
                    		<li>activate or use more than one Member account;</li>
                    		<li>select or use an Email Address of another person;</li>
                    		<li>use a name subject to rights of another person without authorization from that person;</li>
                    		<li>use a false or misleading name (Except for privacy), mailing address, or email address to activate or use a Member account.</li>
                    	</ol>
                    </p>

                    <p>By signing up for the Zeropoverty Income Program, member is opting-in to receive other special offer emails from Zeropoverty  Income Program. If you do not wish to receive these emails, you may cancel your account anytime .</p>

                    <p>Zeropoverty Income Program reserves the right to track Member’s activity by both IP Address as well as individual browser activity.</p>
                    <p>Member agrees not to abuse his or her membership privileges by acting in a manner inconsistent with this Agreement.</p>
                    <p>Member agrees not to attempt to earn through other means than the legitimate channels authorized by Zeropoverty  Income Program.</p>
                    <p>Member agrees not to participate in any fraudulent behavior of any kind.</p>
                    <p>Spamming is strictly prohibited. Any spamming done to advertise Zeropoverty  Income Program will result in immediate termination of your account and a forfeiture of your account earning balance. Incidents will be dealt with on a case by case basis.</p>
                    <p><b>Refund Policy:</b> As we are offering non-tangible virtual digital goods ( Zeropoverty  Income Pack) which is form of registration fee , we do not generally issue refunds after the purchase  of ZIP pack has been made. Please note that by purchasing the ZIP Pack, you agree to the no Refund Policy.</p>

                    <p>Membership activities, posting, sponsored post sharing and commenting:</p>
                    <p>Administrators, Editors and Moderators reserve the right to approve or disapprove contents or comments posted on the Zeropoverty  Forum. As a Zeropoverty  Incomer Program member, you will only earn and get a revenue shared paid to your bank account when you abide by the following rules:</p>
                    <p>Irrelevant contribution (comment) e.g: one word, hmmm, ok, lol, etc and a single line of the same sentence on all post or comments not related to subject matters are not allowed. They will be trashed and you will not earn.</p>
                    <p>Consistent irrelevant post of comments by a user and inappropriate behaviour may result in suspension or termination of membership and forfeiture of members unredeemed earnings.</p>
                    <p>Multiple comment on a post on purpose to earn more is not allowed.</p>
                    <p>Sponsored post sharing must correspond with the post date and must be shared as recommended.</p>

                    <h5>Payment:</h5>
                    <p>We pay out every sunday of the week starting from Zeropoverty  affiliate earners (ZAP) with minimum of at least N2,000 with enough activities earning (ZADS) respectively. Payment shall always start from the highest ZAP earners to others. Once the available revenue is finally exhausted, pending earning will be carried over to the following week  revenue payout.</p>

                    <p>If you did not get paid while you have earned N2,000 threshold, it doesn’t mean you wont get paid while the system keep running. Your earning will be carried over to the following week . You only need to earn more to the top earner for you to have a share on the following month revenue payout.</p>

                    <p>Member’s discontinued participation in the Wakanda Income Program or failure to notify Zeropoverty  Income Program of any address (mailing or email) changes may result in the termination of Member’s membership and forfeiture of Member’s unredeemed Earnings.</p>
                    <p>Member shall comply with all laws, rules, and regulations that are applicable to member. Member acknowledges that Member may only participate in Zeropoverty  Income Program if and to the extent that such participation is permitted by such laws, rules, and regulations.</p>
                    <p>If member objects to any of the Terms and Conditions of this Agreement, or any subsequent modifications to this agreement, or becomes dissatisfied with the Program, Member’s only recourse is to immediately discontinue participation in Wakanda Income Program and properly terminate his or her membership.</p>

                    <h5>Disclaimers</h5>
                    <p>MEMBER EXPRESSLY AGREES THAT USE OF THE SERVICE IS AT MEMBER’S SOLE RISK. THE SERVICE IS PROVIDED ON AN “AS IS” AND “AS AVAILABLE” BASIS. TO THE MAXIMUM EXTENT ALLOWED BY APPLICABLE NIGERIAN LAW, ZEROPOVERTY  Income Program EXPRESSLY DISCLAIMS ALL WARRANTIES OF ANY KIND, EXPRESS OR IMPLIED BY LAW, CUSTOM OR OTHERWISE, INCLUDING WITHOUT LIMITATION ANY WARRANTY OF MERCHANTABILITY, SATISFACTORY QUALITY, FITNESS FOR A PARTICULAR PURPOSE OR NON-INFRINGEMENT. ZEROPOVERTY  Income Program MAKES NO WARRANTY REGARDING ANY GOODS OR SERVICES PURCHASED OR OBTAINED THROUGH THE PROGRAM OR ANY TRANSACTIONS ENTERED INTO THROUGH THE PROGRAM.</p>

                    <p>TO THE MAXIMUM EXTENT ALLOWED BY APPLICABLE NIGERIAN LAW, NEITHER ZEROPOVERTY  Income Program NOR ANY OF ITS MEMBERS, SUBSIDIARIES, PUBLISHERS, SERVICE PROVIDERS, LICENSORS, OFFICERS, DIRECTORS OR EMPLOYEES SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF OR RELATING TO THIS AGREEMENT, RESULTING FROM THE USE OR THE INABILITY TO USE THE SERVICE OR FOR THE COST OF PROCUREMENT OF SUBSTITUTE GOODS AND SERVICES OR RESULTING FROM ANY GOODS OR SERVICES PURCHASED OR OBTAINED OR MESSAGES RECEIVED OR TRANSACTIONS ENTERED INTO THROUGH THE PROGRAM OR RESULTING FROM UNAUTHORIZED ACCESS TO OR ALTERATION OF USER’S TRANSMISSIONS OR DATA, INCLUDING BUT NOT LIMITED TO, DAMAGES FOR LOSS OF PROFITS, USE, DATA OR OTHER INTANGIBLE, EVEN IF SUCH PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.</p>

                    <p>To prevent unauthorized access, maintain data accuracy, and ensure the correct use of information, Zeropoverty  Income Program uses appropriate industry standard procedures to safeguard the confidentiality of Member’s personal information, such as SSL, firewall, encryption, token authentication, application proxies, monitoring technology, and adaptive analysis of the Website’s traffic to track abuse of the Zeropoverty  Website and its data. However, no data transmitted over the Internet can be 100% secure. As a result, while Zeropoverty  Income Program strives to protect its Members personal information, Zeropoverty  Income Program cannot guarantee the security of any information that Members transmit to or from the participating advertisers/merchants and Member does so at his/her own risk.</p>
                    <p>This Agreement constitutes the entire Agreement between Member and Zeropoverty  Income Program in connection with general membership in the Zeropoverty  Income Program and supersedes all prior agreements between the parties regarding the subject matter contained herein. If any provision of this AGREEMENT is found invalid or unenforceable, that provision will be enforced to the maximum extent permissible, and the other provisions of this AGREEMENT will remain in force. Failure of either party to exercise or enforce any of its rights under this AGREEMENT, within two(2) months the cause arose, will act as a waiver of such rights. In the event of any dispute or need for interpretation or enforcement of terms, arising out of this agreement, parties shall refer to arbitration before litigation.</p>
                </div>
            </div>
        </div>
    </div>
</div>
@include('footer-main')
@endsection
