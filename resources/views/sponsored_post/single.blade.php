@extends('layouts.app3')

@section('content')
@section('title')
  Sponsor Post - {{ $post->title }}
@endsection

    @section('css')
        <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
    @stop
<div class="container" id="login">
    <div class="row">
        <div class="col-md-8">
            <div class="panel">

                <div class="panel-body">
                    <p><strong>{{ $post->title }}</strong>
                        <br>
                    <span>
                        <small>
                            
                            By {{ $post->admin->username }} | 

                            {{ $post->created_at->diffForHumans() }} |

                            <span><i class="fa fa-eye"></i> {{$viewCount}} views | </span>

                            <span> <i class="fa fa-comments-o"></i> {{\App\Models\SponsoredComment::countPostComment($post->slug)}} comments </span>
                        </small>
                    </span>
                    </p>
                    <p style="text-align: justify;">{!! $post->body !!}

                        <br><br>
                        <span style="float: right;">@include('social-share')</span>
                    </p>

                </div>
            </div>

            <br>

            <div class="panel">

                <div class="panel-header" style="background: #8f5297; color: #fff;">
                    @if(count($comments) > 0)
                        {{ \App\Models\SponsoredComment::countPostComment($post->slug) }} replies | Last Update {{ $lastComment->created_at->diffForHumans() }} | Last Comment {{  $lastComment->user->name }}
                    @endif
                </div>
                <div class="panel-body">
                    @if(count($comments)  > 0)
                        @foreach($comments as $comment)
                            <div style="border: 1px solid #bcb8b8; padding-bottom: 10px; padding-right: 10px; padding-left: 10px; padding-top: 10px;">
                                <p>
                                    @if($comment->user->image == null)
                                        <img src="{{asset('img/profile_avatar.jpeg')}}" style="border-radius: 50%; padding-right: 2px;"> <!-- Link to Images  -->
                                    @else
                                        <img src="{{$comment->user->image}}" style="border-radius: 50%; padding-right: 2px;">
                                    @endif
                                    <span><small style="font-size: 1.10em;"> <b>{{ $comment->user->name}}</b> | {{ $comment->created_at->diffForHumans() }}</small></span></p>
                                <p>{!! $comment->body !!}</p>
                                @if(Auth::check())
                                    @if(Auth::user()->id == $comment->user_id)
                                        <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#commentModal" onclick="getEditModal({{$comment->id}})">Edit</a>
                                        
                                    @endif
                                @endif
                            </div>

                            {{$comments->links()}}
                        @endforeach

                        <div class="modal fade" id="commentModal" tabindex="-1" role="dialog" aria-labelledby="commentModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form method="PUT" action="{{url('sponsor/post/'.$post->slug.'/comment/'.$comment->id)}}">
                                            <div class="col-md-12 col-sm-8">
                                                <textarea class="form-check-input" name="body" id="summernoteEdit" ></textarea>
                                            </div>
                                            <button type="button" class="btn btn-primary">Save</button>
                                        </form>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @else
                        <h4>Be the first to comment</h4>

                        
                    @endif

                </div>
            </div>

            <br>
                @if(Auth::check())
                    @if($post->created_at >= \Carbon\Carbon::now()->subDay())
                        <div class="panel">
                            <div class="panel-body">
                                <form method="POST" action="{{url('sponsor/comment/'. $post->slug)}}">
                                    @csrf
                                    <div class="col-md-12 col-sm-8 ">
                                        <textarea class="form-check-input" name="body" id="summernote" ></textarea>
                                    </div>
                                        <br>
                                    <button type="submit" class="btn btn-primary">Comment</button>
                                </form>
                            </div>
                        </div>
                    @else
                        <h4>You cannot comment on this post</h4>
                    @endif
                @else
                    <h4>You need to be logged In</h4>
                @endif
        </div>

        <div class="col-md-4">
        	<div class="panel">
        		<!-- <div class="panel-body">
        			
        		</div> -->
        	</div>
        	<div class="panel" style="top: 80px;">
        		<div class="panel-header" style="background: #8f5297; color: #fff;">
        			Quick User Earnings
        		</div>
        		<div class="panel-body">
        			@guest
        				<p> You need to be logged in to see your earnings!</p>
        			@else
        				<ul style="list-style: none;">
        					@foreach($activities as $activity)
        						<li style="font-size: 1.10em; font-weight: 700; border: 1px solid #bcb8b8; margin-bottom: 3px; padding-left: 4px;">&#8358;{{ $activity->amount_earned }} - {{ $activity->activity_type }} - {{ $activity->created_at->diffForHumans() }} </li>
        					@endforeach
        				</ul>
        			@endguest
        		</div>
        	</div>
        </div>
    </div>
</div>
@include('footer-main')

@section('js')
    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>   
 
    <script>
        $(document).ready(function() {
            $('#summernote').summernote({ 
                placeholder: 'Enter your comment here ...',
                height: 250
            });
        });

        function getEditModal(id)
        {
            let id = id;
            let slug = "{{ $post->slug }}";
    
            $.ajax({
              type: 'GET',
              url: '/sponsor/post/'+slug+'/comment/'+id,
              success: function(data){

                let response = JSON.parse(data);

                if(response.status === 200)
                {
                    let body = response.response.body;
                    
                    $("#summernoteEdit").summernote('code', body); 

                }

              },
              error: function(jqXHR){

                
              },
            });  
        }
    </script>

@stop
@endsection
