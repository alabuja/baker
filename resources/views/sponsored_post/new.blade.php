@extends('layouts.app-admin')

@section('content')

	@section('admin-css')
	<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
    @stop
<!-- ============ Body content start ============= -->
        <div class="main-content-wrap sidenav-open d-flex flex-column">
            <div class="breadcrumb">
                <h1>Zero Poverty</h1>
                <ul>
                    <li><a href="{{url('admin/dashboard')}}">Dashboard</a></li>
                    <li>Posts</li>
                </ul>
            </div>

            <div class="separator-breadcrumb border-top"></div>
			<div class="row">
                <div class="col-md-12">
                    <h4>Create Posts</h4>
                    <div class="card">
                        <div class="card-body">
                            <form method="POST" action="{{ url('admin/sponsor/new') }}" aria-label="Create Posts" enctype="multipart/form-data">
                            	@csrf
                                <div class="form-group row">
                                    <label for="title" class="col-sm-2 col-md-2 col-form-label">{{ __('Title') }}</label>
                                    <div class="col-sm-10 col-md-6">
			                            <textarea rows="3" cols="7" id="title" class="form-control" name="title" required></textarea>
			                        </div>
                                </div>
                                <div class="form-group row">
		                            <label for="body" class="col-sm-2 col-md-2 col-form-label">{{ __('Body') }}</label>

		                            <div class="col-sm-10 col-md-8">
		                                <textarea id="summernote" name="body" required></textarea>
		                            </div>
		                        </div>

		                        <div class="form-group row">
                                	<div class="col-md-6 col-sm-10">
				                        <input id="image" type="file" class="form-control{{ $errors->has('image') ? ' is-invalid' : '' }}" name="image"  required>

				                        @if ($errors->has('image'))
				                            <span class="invalid-feedback" role="alert">
				                                <strong>{{ $errors->first('image') }}</strong>
				                            </span> 
				                        @endif
				                    </div>
		                        </div>

                                <div class="form-group row">
                                    <div class="col-sm-10">
                                        <button type="submit" class="btn btn-primary">{{ __('Create Sponsored Post') }}</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            @include('footer')
        </div>
        <!-- ============ Body content End ============= -->

@section('admin-js')
    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>     
 
    <script>
        $(document).ready(function() {
            $('#summernote').summernote({
                height: 250
            });
        });
    </script>

@stop

@endsection