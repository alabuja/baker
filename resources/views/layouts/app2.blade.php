<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="keyword" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <title>Zero Poverty | @yield('title')</title>
    <link rel="icon" type="image/png" sizes="16x16" href="{{url('img/logo.png')}}">
  <!-- core CSS -->
    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/animate.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/prettyPhoto.css')}}" rel="stylesheet">
    <link href="{{asset('css/style.css')}}" rel="stylesheet">
    <link href="{{asset('css/responsive.css')}}" rel="stylesheet">

    @yield('css')
    <style type="text/css">
        .footer-bottom{
            bottom: 0;
            width: 100%;
        }
    </style>
</head><!--/head-->

<body class="homepage">

    <header id="header">
        <nav class="navbar navbar-default navbar-fixed-top" role="navigation" style="padding: 1em; background: #1c1b1e;">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="{{url('/')}}"><img src="{{asset('img/rsz_image_13.png')}}" alt="logo"></a>
                </div>
         
                <div class="collapse navbar-collapse navbar-right">
                    <ul class="nav navbar-nav">
                        @guest
                            <li class="{{ Request::is('users/login') ? 'active' : '' }}">
                        		<a href="{{ url('users/login') }}">{{ __('Login') }}</a>
                    		</li>
                    		<li class="{{ Request::is('register') ? 'active' : '' }}">
                        		<a href="{{ url('register') }}">{{ __('Register') }}</a>
                    		</li> 
                        @else
                        	<li class="dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown">
                                    {{ Auth::user()->name }} <i class="fa fa-btn fa-caret-down"></i>
                                </a>

                                <ul class="dropdown-menu">
                                    
                                    <li> 
                                    	<a href="{{ url('users/dashboard')}}">{{ __('Dashboard') }}</a> </li>
                                    <li>
                                        <a href="{{ url('users/logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ url('users/logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                    </li>
                                </ul>
                            </li>
                        @endguest
                        <li class="{{ Request::is('how-to-make-money') ? 'active' : '' }}">
                    		<a href="{{ url('how-to-make-money') }}">{{ __('Make Money Here') }}</a>
                		</li>
                		<li>
                    		<a>{{ \App\Models\User::countUsers() }} {{ __('Members') }}</a>
                		</li>
                    </ul>
                </div>
            </div><!--/.container-->
        </nav><!--/nav-->
    
    </header><!--/header-->

    @yield('content')


    <script src="{{asset('js/jquery.js')}}"></script>
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/jquery.prettyPhoto.js')}}"></script>
    <script src="{{asset('js/jquery.isotope.min.js')}}"></script>
    <script src="{{asset('js/main.js')}}"></script>
    <script src="{{asset('js/wow.min.js')}}"></script>
    <script src="{{asset('js/custom.js')}}"></script>
    <script src="{{ asset('js/share.js') }}"></script>
    @yield('js')
</body>
</html>