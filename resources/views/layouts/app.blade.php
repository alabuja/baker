<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Zero Poverty') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <!-- <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet"> -->
    <link href="{{asset('css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">

    <!-- GOOGLE FONTS -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300%7CRoboto:400,100,300,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="{{ asset('css/style.css')}}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{ asset('css/responsive.css')}}">

    @yield('css')

</head>
<body class="aboutus-template">
    <header class="header"> 
  <!-- Topbar -->
  <div class="header-topbar">
    <div class="container">
      <div class="row"> 
        
        <!-- Social Links -->
        <div class="social-icon-links">
          <ul>
            <li class="fb-icon"> <a href="#"> <i class="fa fa-facebook"></i> </a> </li>
            <li class="twitter-icon"> <a href="#"> <i class="fa fa-twitter"></i> </a> </li>
            <li class="behance-icon"> <a href="#"> <i class="fa fa-behance"></i> </a> </li>
            <li class="dribbble-icon"> <a href="#"> <i class="fa fa-dribbble"></i> </a> </li>
            <li class="vimeo-icon"> <a href="#"> <i class="fa fa-vimeo"></i> </a> </li>
          </ul>
        </div>
        <!-- End --> 
      </div>
    </div>
  </div>
  <!-- End --> 
  
  <!--main header -->
  <div class="header-mainbar"> 
    <!-- Navigation Menu start-->
    <nav class="navbar main-menu" role="navigation">
      <div class="container">
        <div class="row"> 
          <!-- Navbar Toggle -->
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
            
            <!-- Logo --> 
            <a class="navbar-brand" href="{{url('/')}}"><img class="logo" src="images/logo-header.png" alt="ZERO Poverty"></a> </div>
          <!-- Navbar Toggle End --> 
          
          <!-- navbar-collapse start-->
          <div id="nav-menu" class="navbar-collapse menu-wrapper collapse" role="navigation"> 
            <!-- Menu -->
            <ul class="nav navbar-nav menus">

                @guest
                    <li class="">
                        <a href="{{ url('users/login') }}">{{ __('Login') }}</a>
                    </li>
                    <li class="">
                        <a class="nav-link" href="{{ url('register') }}">{{ __('Register') }}</a>
                    </li>
                @else
                @endguest
                <li class="">
                    <a class="nav-link" href="{{ url('how-to-make-money') }}">{{ __('Make Money Here') }}</a>
                </li>
                <li class="">
                    <a class="">{{ \App\Models\User::countUsers() }} {{ __('Members') }}</a>
                </li>
            </ul>
              <li class="active"> <a href="about-us.html">About</a> </li>
               <li> <a href="works.html">Our Works</a> </li>
              <li> <a href="blog.html">Blog</a>
                <ul class="dropdown-menu">
                  <li> <a href="blog-details.html">Blog Details</a> </li>
                </ul>
              </li>
              <li> <a href="contact-us.html">Contact</a> </li>
            </ul>
            <!-- End --> 
          </div>
          <!-- navbar-collapse end--> 
          
          <!-- Search -->
          <div class="search-wrapper"> <a href="#" class="search-icon"> <i class="fa fa-search"></i> </a> </div>
          <!-- End --> 
        </div>
      </div>
    </nav>
    <!-- Navigation Menu end--> 
  </div>
  <!-- End --> 
</header>
<!-- HEADER END --> 
    <!-- <header id="header"> -->
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Zero Poverty') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">

                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ url('users/login') }}">{{ __('Login') }}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ url('register') }}">{{ __('Register') }}</a>
                            </li>
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    
                                    <li> <a class="dropdown-item" href="{{ url('users/dashboard')}}">{{ __('Dashboard') }}</a> </li>
                                    <li>
                                        <a class="dropdown-item" href="{{ url('users/logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ url('users/logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                    </li>
                                </ul>
                            </li>

                        @endguest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ url('how-to-make-money') }}">{{ __('Make Money Here') }}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link">{{ \App\Models\User::countUsers() }} {{ __('Members') }}</a>
                            </li>
                    </ul>
                </div>
            </div>
        </nav>
            
    <!-- </header> -->

    <main class="py-4">
            @yield('content')
        </main>
    </div>

    @yield('js')

    <script src="{{asset('js/vendors/jquery.min.js')}}"></script>
    <!-- Plugins --> 
    <script src="{{asset('js/plugins.js')}}"></script>
    <!-- Main JS --> 
    <script src="{{asset('js/main.js')}}"></script>
</body>
</html>
