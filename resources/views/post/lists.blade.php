@extends('layouts.app-admin')

@section('content')
		@section('admin-css')
    		<link rel="stylesheet" href="{{ asset('css/datatables.min.css') }}">

   		 @stop
<!-- ============ Body content start ============= -->
        <div class="main-content-wrap sidenav-open d-flex flex-column">
            <div class="breadcrumb">
                <h1>Zero Poverty</h1>
                <ul>
                    <li><a href="{{url('admin/dashboard')}}">Dashboard</a></li>
                    <li>Posts</li>
                </ul>
            </div>

            <div class="row">
            	<div class="col-md-12">
            		<a class="btn btn-primary text-white float-right" href="{{ url('admin/posts/create')}}"> New Post</a>
            	</div>
            </div>

            <div class="separator-breadcrumb border-top"></div>
                <div class="row mb-4">
                	<div class="col-md-12 mb-4">
                		@include("alerts")
                    <div class="card text-left">

                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="zero_configuration_table" class="display table table-striped table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>S/N</th>
	                                		<th>Title</th>
	                                		<th>Body</th>
	                                		<th>Submitted By</th>
	                                		<th>Date Created</th>
	                                		<th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($posts as $key => $post)
                                        	<tr>
	                            				<td>{{++$key}}</td>
	                            				<td>{{ $post->title}}</td>
	                            				<td>{!! $post->body !!}</td>
	                            				<td>{{ $post->user->name }}</td>
	                                            <td>{{ $post->created_at->toDateString() }}</td>
	                                            <td>
	                                            	@if($post->isApproved ==  false)
	                                            		<a href="{{ url('admin/posts/approve/'. $post->id ) }}" class="btn btn-success">Approve</a>
	                                            	@endif
	                                            	@if($post->isApproved ==  true)
	                                            		<a href="{{ url('admin/posts/reject/'. $post->id ) }}" class="btn btn-info">Reject</a>
	                                            	@endif
	                                            	<a href="{{ url('admin/posts/delete/'. $post->id ) }}" class="btn btn-danger">Delete</a>
	                                            </td>
	                                        </tr>
                                        @endforeach
                                        
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>S/N</th>
	                                		<th>Title</th>
	                                		<th>Body</th>
	                                		<th>Submitted By</th>
	                                		<th>Date Created</th>
	                                		<th>Action</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>

                        </div>
                    </div>
                	</div>

                <!-- end of col -->
            	</div>

            @include('footer')
        </div>
        <!-- ============ Body content End ============= -->

   		 @section('admin-js')
    		<script src="{{ asset('js/datatables.min.js') }}"></script>
    		<script src="{{ asset('js/datatables.script.js') }}"></script>

   		@stop
@endsection