@extends('layouts.app-dashboard')

@section('content')
		@section('css')
    		<link rel="stylesheet" href="{{ asset('css/datatables.min.css') }}">

   		 @stop
<!-- ============ Body content start ============= -->
        <div class="main-content-wrap sidenav-open d-flex flex-column">
            <div class="breadcrumb">
                <h1>Zero Poverty</h1>
                <ul>
                    <li><a href="{{url('users/dashboard')}}">Dashboard</a></li>
                    <li>Activities</li>
                </ul>
            </div>

            <div class="separator-breadcrumb border-top"></div>
                <div class="row mb-4">
                	<div class="col-md-12 mb-4">
                    <div class="card text-left">

                        <div class="card-body">
                            <h4 class="card-title mb-3">Earnings</h4>
                            <div class="table-responsive">
                                <table id="zero_configuration_table" class="display table table-striped table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>S/N</th>
                                            <th>Activity</th>
                                            <th>Earned</th>
                                            <th>Time</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($activities as $key => $activity)
                                        	<tr>
	                                            <td>{{ ++$key }}</td>
	                                            <td>{{ $activity->activity_type }}</td>
	                                            <td>&#8358;{{ $activity->amount_earned }}</td>
	                                            <td>{{ $activity->created_at->diffForHumans() }}</td>
	                                        </tr>
                                        @endforeach
                                        
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>S/N</th>
                                            <th>Activity</th>
                                            <th>Earned</th>
                                            <th>Time</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>

                        </div>
                    </div>
                	</div>

                <!-- end of col -->
            	</div>

            @include('footer')
        </div>
        <!-- ============ Body content End ============= -->

   		 @section('js')
    		<script src="{{ asset('js/datatables.min.js') }}"></script>
    		<script src="{{ asset('js/datatables.script.js') }}"></script>

   		@stop
@endsection