@extends('layouts.app-dashboard')

@section('content')
<!-- ============ Body content start ============= -->
        <div class="main-content-wrap sidenav-open d-flex flex-column">
            <div class="breadcrumb">
                <h1>Zero Poverty</h1>
                <ul>
                    <li><a href="{{url('users/dashboard')}}">Dashboard</a></li>
                    <li>Sponsor Post</li>
                </ul>
            </div>

            <div class="separator-breadcrumb border-top"></div>

            <div class="row">
                <div class="col-md-6">
                    <div class="card mb-5">
                        <div class="card-body">
                            <div class="form-group row">
                            	@if($post != null)
                                <div class="col-sm-10 col-md-12">
                                	<h3>{{ $post->title }}</h3>
	                                <a href="{{ url('sponsor/post/'.$post->slug) }}">
	                                	<img src="{{ $post->image_url }}">
	                                </a>
                                	<p style="text-align: justify;">{!! substr(strip_tags($post->body), 0, 250) !!} {!! strlen(strip_tags($post->body)) > 250 ? "..." : "" !!}</p>
                                	<small><i class="fa fa-comments-o"></i>{{\App\Models\SponsoredComment::countPostComment($post->slug)}} comments</small>
			                    </div>
			                    @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            @include('footer')
        </div>
        <!-- ============ Body content End ============= -->
@endsection