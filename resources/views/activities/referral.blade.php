@extends('layouts.app-dashboard')

@section('content')
		@section('css')
    		<link rel="stylesheet" href="{{ asset('css/datatables.min.css') }}">

   		 @stop
<!-- ============ Body content start ============= -->
        <div class="main-content-wrap sidenav-open d-flex flex-column">
            <div class="breadcrumb">
                <h1>Zero Poverty</h1>
                <ul>
                    <li><a href="{{url('users/dashboard')}}">Dashboard</a></li>
                    <li>Referrals</li>
                </ul>
            </div>

            <div class="separator-breadcrumb border-top"></div>
                <div class="row mb-4">
                	<div class="col-md-12 mb-4">
                    <div class="card text-left">

                        <div class="card-body">
                            <h4 class="card-title mb-3">Earnings</h4>
                            <div class="table-responsive">
                                <table id="zero_configuration_table" class="display table table-striped table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>S/N</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Bonus</th>
                                            <th>Package</th>
                                            <th>Status</th>
                                            <th>Date</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($referrals as $key => $referral)
                                        	<tr>
	                                            <td>{{ ++$key }}</td>
	                                            <td>{{ $referral->name }}</td>
	                                            <td>{{ $referral->email }}</td>
	                                            <td>&#8358;{{ $referral->bonus }}</td>
	                                            <td>{{ $referral->package_type }}</td>
	                                            <td>
	                                            	@if($referral->has_paid == false)
	                                            	<span style="color: #ff0000;"> {{ __('UNPAID') }} </span>
	                                            	@else
	                                            	<span style="color: #1f890f;">{{ __('PAID') }}</span>
	                                            	@endif
	                                            </td>
	                                            <td>{{ $referral->created_at->toDateString() }}</td>
	                                        </tr>
                                        @endforeach
                                        
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>S/N</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Bonus</th>
                                            <th>Package</th>
                                            <th>Status</th>
                                            <th>Date</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>

                        </div>
                    </div>
                	</div>

                <!-- end of col -->
            	</div>

            @include('footer')
        </div>
        <!-- ============ Body content End ============= -->

   		 @section('js')
    		<script src="{{ asset('js/datatables.min.js') }}"></script>
    		<script src="{{ asset('js/datatables.script.js') }}"></script>

   		@stop
@endsection