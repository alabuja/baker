@extends('layouts.app3')

@section('content')
@section('title')
  Privacy & Policy
@endsection
<div class="container" id="login">
    <div class="row">
        <div class="col-md-8">
            <div class="panel">

                <div class="panel-body">
                    <h3>Privacy Policy</h3>
                    <br>
                    <p>The privacy of our visitors is extremely important and we take it very serious because we respect our visitor. This privacy outlines the types of personal information that is received and collected and how it been processed.</p>

                    <p>Please note that our privacy policy is been reviewed and revised from time to time. You will want to re-visit it regularly. Your use of this website, in any and all forms, constitute an acceptance of all privacy policy.</p>

                    <p><b>Who are we?</b></p>

                    <p>Our website address is http://zeropoverty.ng</p>

                    <br>

                    <h5 style="color: #ff0000;">What Personal data we collect and why we collect it</h5>

                    <p><b>Comment</b></p>
                    <p>When visitors leave comments on the site, we collect the data shown in the comment form, and also the visitor’s IP address and browser user agent string to help spam detection.</p>

                    <p><b>Embedded content from other website</b></p>

                    <p>Articles on this site may include embedded content (e.g. Video, Images, articles, etc). Embedded content from other website behaves in the exact same way as if the visitor has visited the other website.</p>

                    <p>These website may collect data about you, use cookies, embed additional third-party tracking, and monitor your interaction with that embedded content, including tracking your interaction with the embedded content if you have if you have an account and are logged in to that website.</p>

                    <p><b>Cookies</b></p>

                    <p>If you leave a comment on our website, you may opt-in to saving your name, email address and website in cookies. These are your convenience so that you do not have to fill in your details again when you leave another comment. These cookies will last for one year.</p>

                    <p><b>How Long we retain your data</b></p>
                    <p>If you leave a comment, the comment and its metadata are retained indefinitely. This is so we can recognise and approve any follow-up comments automatically instead of holding them in a moderation queue.</p>

                    <p><b>What right you have over your data</b></p>

                    <p>If you left comments, you can request to receive an exported file of the personal data we hold about you, including any data you have provided to us. you can also request that we erase any personal data we hold about you. This does not include any data we obliged to keep for administrative, legal, or security purposes.</p>

                    <p><b>Where we send your data</b></p>

                    <p>Visitor comments may be checked through an automated spam detection service.</p>
                </div>
            </div>
        </div>
    </div>
</div>
@include('footer-main')
@endsection
