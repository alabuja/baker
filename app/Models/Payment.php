<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    public function allPayments()
    {
    	$payments  	=	self::whereNotNull('payment_reference')->get();

    	return $payments;
    }

    public function user()
    {
    	return $this->belongsTo('App\Models\User');
    }
}
