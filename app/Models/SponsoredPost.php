<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use App\Models\SponsoredComment;
use Carbon\Carbon;

class SponsoredPost extends Model
{
	use Sluggable;

    protected $table = "sponsored_posts";

	/**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public function admin()
    {
        return $this->belongsTo('App\Models\Admin');
    }

    public function sponsored_comment()
    {
        return $this->hasMany('App\Models\SponsoredComment');
    }

    public function sponsoredPost()
    {
    	$posts = self::select('*')
                ->where('created_at', '>=', Carbon::now()->subDay())
                ->first();

    	return $posts;
    }

    public static function countPosts()
    {
        $posts     =   self::count();

        return $posts;
    }

    public function getCommentsBySponsoredPost($slug)
    {
        $post = self::where('slug', $slug)->first();

        $postId = $post->id;

        $comments = SponsoredComment::where('sponsored_post_id', $postId)->get();

        return $comments;
    }

    public function mySponsoredPost()
    {
        $posts = self::where('admin_id', Auth::guard('admin')->user()->id)
                        ->get();

        return $posts;
    }
}
