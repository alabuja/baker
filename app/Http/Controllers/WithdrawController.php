<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Withdraw;

class WithdrawController extends Controller
{
	public function __construct(Withdraw $withdraw)
	{
		$this->withdraw = $withdraw;
	}

    public function setWithDrawal()
    {
        $withdrawal = $this->withdraw->first();

        return view('admin.setWithDrawal', compact('withdrawal'));
    }

    public function open(Request $request)
    {
        $this->validate($request, [
            'status' => 'required'
        ]);

    	$status  =  $request->status;

    	$this->withdraw::where('id', 1)->update(['status' => $status]);
    	
    	$request->session()->flash('success', 'Withdrawal Status for the week is opened');

    	return back();
    }

    public function close(Request $request)
    {
    	$this->validate($request, [
            'status' => 'required'
        ]);

        $status  =  $request->status;

    	$this->withdraw::where('id', 1)->update(['status' => $status]);

    	$request->session()->flash('success', 'Withdrawal Status for the week is closed');

    	return back();
    }
}
