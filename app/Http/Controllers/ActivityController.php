<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Activity;

class ActivityController extends Controller
{
	public function __construct(Activity $activity)
	{
		$this->activity = $activity;
	}

    public function show()
    {
    	$results = 	[
    					'activities' => $this->activity->activities()
    				];

    	return view('activities.show', $results);
    }
}
