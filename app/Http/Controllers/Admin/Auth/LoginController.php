<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = 'admin/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function getLoginForm()
    {
        return view('admin.auth.login');
    }

    public function authenticate(Request $requests)
    {
        $email = $requests->input('email');
        $password = $requests->input('password');

        if(auth()->guard('admin')->attempt(['email' => $email, 'password' => $password], $requests->has('remember') ))
        {
            return redirect()->intended('admin/dashboard');
        }
        
        $requests->session()->flash('failure', 'Invalid Login Credentials');
        return redirect()->intended('admin/login');
    }

    public function logout(Request $request)
    {
        auth()->guard('admin')->logout();
        $request->session()->invalidate();
        
        return redirect()->intended('admin/login');
    }
}
