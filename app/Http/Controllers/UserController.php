<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Models\Activity;
use App\Models\Referral;
use App\Models\Post;
use App\Models\SponsoredPost;
use App\Models\PaymentRequest;
use App\Models\Withdraw;
use App\Models\BankInfo;
use Image;
use Carbon\Carbon;

class UserController extends Controller
{
    public function __construct(User $user, Referral $referral, Activity $activity, Post $post, SponsoredPost $sponsoredPost, Withdraw $withdraw, BankInfo $bankInfo, PaymentRequest $paymentRequest)
	{
        $this->user = $user;
        $this->referral = $referral;
		$this->activity = $activity;
        $this->post = $post;
        $this->sponsoredPost = $sponsoredPost;
        $this->withdraw = $withdraw;
        $this->bankInfo = $bankInfo;
        $this->paymentRequest = $paymentRequest;
	}

    public function timeline()
    {
        $userId = Auth::user()->id;

        $user = $this->user->find($userId);

        $bankInfo = $user->bankInfo;

        return view('users.timeline', compact('user', 'bankInfo'));
    }

    public function profile()
    {
        $user = $this->user->find(Auth::user()->id);

        return view('users.profile', compact('user'));
    }

    public function update(Request $request)
    {
    	$this->validate($request, [
    		'name' 				=>  'required|string',
    		'email' 			=> 	'required|string',
    		'phone_number' 		=> 	'nullable',
    		'mobile_number' 	=> 	'nullable',
    		'date_of_birth'  	=> 	'nullable|date',
    		'city'				=> 	'nullable',
    		'address'			=>	'nullable',
    		'facebook_username'	=>	'nullable',
    		'twitter_username'	=>	'nullable',
    		'google_username'	=>	'nullable',
    		'signature'			=>	'nullable',
    		'gender'			=>	'nullable',
    		'about_me'			=>	'nullable',
    		'image'				=>	'nullable'
    	]);

    	$userId = Auth::user()->id;

    	$name 					= $request->name;
    	$email 					= $request->email;
    	$phoneNumber  			= $request->phone_number;
    	$mobileNumber 			= $request->mobile_number;
    	$dateOfBirth 			= $request->date_of_birth;
    	$city 					= $request->city;
    	$facebookUsername 		= $request->facebook_username;
    	$twitterUsername 		= $request->twitter_username;
    	$googleUsername 		= $request->google_username;
    	$signature 				= $request->signature;
    	$gender 				= $request->gender;
    	$about_me 				= $request->about_me;

        $filename = '';
    	if ($request->hasFile('image')) {
            $image = $request->file('image');
            $filename = time() . '.' . $image->getClientOriginalExtension();
            Image::make($image)->resize(300, 300)->save(public_path('/users/' . $filename));
        }

    	$this->user::where('id', $userId)->update([
    		'name' => $name, 'email' => $email,
    		'phone_number' => $phoneNumber, 'mobile_number' => $mobileNumber,
    		'date_of_birth' => $dateOfBirth, 'city' => $city, 
    		'facebook_username' => $facebookUsername, 
    		'twitter_username' => $twitterUsername, 
    		'google_username' => $googleUsername, 'signature' => $signature, 
    		'gender' => $gender, 'about_me' => $about_me, 'image' => $filename
    	]);

    	$request->session()->flash('success', 'You just updated your profile');

        return back();
    }

    public function password()
    {
    	return view('users.password');
    }

    public function dashboard()
    {
        $referrals          = $this->referral->referralEarnings();
        $currentEarnings    = $this->activity->currentActivityEarnings();
        $allTimeEarnings    = $this->activity->allTimeEarnings();

        return view('users.dashboard', compact('referrals', 'currentEarnings', 'allTimeEarnings'));
    }

    public function updatePassword(Request $request)
    {
    	$validator = $this->validate($request, [
            'old' => 'required',
            'password' => 'required|confirmed',
        ]);

        $user = $this->user->find(Auth::id());
        $hashedPassword = $user->password;

 
        if (Hash::check($request->old, $hashedPassword)) 
        {
            $user->fill([
                'password' => Hash::make($request->password)
            ])->save();
 
            return redirect()->back()->with('success', 'Your password has been changed.');
        }
 
        return redirect()->back()->withErrors($validator);
    }

    public function myReferrals()
    {
        $referrals = $this->referral->myReferrals();

        return view('activities.referral', compact('referrals'));
    }

    public function sponsoredPost()
    {
        $post = $this->sponsoredPost->sponsoredPost();

        return view('activities.sponsored_post', compact('post'));
    }

    public function withdraw()
    {
        $referrals          =   $this->referral->referralEarnings();
        $withDrawalStatus   =   $this->withdraw->where('id', 1)->first();
        $currentAmount      =   $this->activity->currentActivityEarnings();
        $user               =   $this->user->where('id', Auth::user()->id)->first();   
        $paymentRequests    =   $this->paymentRequest->myWithdrawalRequests(); 

        return view('users.withdraw', [
            'withDrawalStatus'  =>  $withDrawalStatus,
            'currentAmount'     =>  $currentAmount,
            'user'              =>  $user,
            'referrals'         =>  $referrals,
            'paymentRequests'   =>  $paymentRequests
        ]);
    }

    public function upgrade()
    {
        $user = $this->user->find(Auth::user()->id);

        return view('users.upgrade', compact('user'));
    }
}
