<?php

namespace App\Http\Controllers\Auth;

use App\Models\Activity;
use App\Models\LoginHistory;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function getLoginForm()
    {
        return view('auth.login');
    }

    public function authenticate(Request $request)
    {
        $activity = new Activity;
        $loginHistory = new LoginHistory;

        $email = $request->input('email');
        $password = $request->input('password');

        if(auth()->attempt(['email' => $email, 'password' => $password], $request->has('remember') ))
        {
                $userId = Auth::user()->id;
                //dd($userId);
                $login = $loginHistory::where('user_id', $userId)->orderBy('id', 'desc')->first();
                
                if( Carbon::today()->toDateString() > $login->date_time)
                {
                    $activity->user_id       =  $userId;
                    $activity->activity_type = 'login';
                    $activity->amount_earned = '70';
                    $activity->save();

                    $loginHistory->date_time    =   Carbon::today();
                    $loginHistory->user_id      =   $userId;
                    $loginHistory->save();
                }

            return redirect()->intended('/');
        }
        
        $request->session()->flash('failure', 'Invalid Login Credentials');
        return redirect()->intended('users/login');
    }

    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();
        return redirect('users/login');
    }
}
