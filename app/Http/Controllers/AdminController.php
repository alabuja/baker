<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Models\Post;
use App\Models\Admin;
use App\Models\User;
use App\Models\Category;
use Cloudder;

class AdminController extends Controller
{
    public function __construct(Post $post, Admin $admin, Category $category, User $user)
	{
        $this->post = $post;
        $this->admin = $admin;
        $this->category = $category;
		$this->user = $user;
	}

    public function dashboard()
    {
        return view('admin.dashboard');
    }

    public function create()
    {
        $categories = $this->category->categories();

        return view('admin.post.create', compact('categories'));
    }

    public function profile()
    {
        return view('admin.profile');
    }

    public function password()
    {
        return view('admin.password');
    }

    public function updatePassword(Request $request)
    {
        $validator = $this->validate($request, [
            'old' => 'required',
            'password' => 'required|confirmed',
        ]);

        $user = $this->admin->find(Auth::guard('admin')->id());
        $hashedPassword = $user->password;

 
        if (Hash::check($request->old, $hashedPassword)) 
        {
            $user->fill([
                'password' => Hash::make($request->password)
            ])->save();
 
            return redirect()->back()->with('success', 'Your password has been changed.');
        }
 
        return redirect()->back()->withErrors($validator);
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'name'              =>  'required|string',
            'email'             =>  'required|string',
            'image'             =>  'required|mimes:jpeg,jpg,png'
        ]);

        if($request->file('image') != null)
        {
            $image                          =   $request->file('image')->getRealPath();

            Cloudder::upload($image, null);
            list($width, $height)                = getimagesize($image);

            $url = Cloudder::secureShow(Cloudder::getPublicId(), [
                "crop" => "fit", "width" => 200, "height" => 200
            ]);
        }
        

        $name          = $request->name;
        $email         = $request->email;

        $this->admin::where('id', Auth::guard('admin')->user()->id)->update([
            'name' => $name, 'email' => $email,
            'image_url' => $url
        ]);

        $request->session()->flash('success', 'You just updated your profile');

        return back();
    }

	public function store(Request $request)
    {
    	$this->validate($request, [
    		'title'       	=> 	'required',
    		'category_id' 	=> 	'nullable',
    		'body'			=>	'required',
    		'post_type'		=>	'required' // admin, pin
    	]);

    	$userId = Auth::guard('admin')->user()->id;

    	$this->post->title =  $request->title;
    	$this->post->category_id = $request->category_id;
    	$this->post->admin_id = $userId;
    	$this->post->body = $request->body;
    	$this->post->post_type = $request->post_type;
        $this->post->isApproved = true;

    	$this->post->save();

    	$request->session()->flash('success', 'Post Created Successfully');

    	return back();
    }

    public function viewUser($id)
    {
        return view('admin.single-user');
    }

    public function viewUsers()
    {
        $users = $this->user->getUsers();

        return view('admin.users', compact('users'));
    }

}
