<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Testimony;

class TestimonyController extends Controller
{
    public function __construct(Testimony $testimony)
	{
		$this->testimony = $testimony;
	}

	public function store(Request $request)
    {
    	$this->validate($request, [
    		'rating'	=>	'required',
    		'message'	=>	'required'
    	]);

    	$userId = Auth::user()->id;

    	$this->testimony->rating  	= 	$request->rating;
    	$this->testimony->message  	=	$request->message;
    	$this->testimony->user_id	=	$userId;
    	$this->testimony->save()

    	$request->session()->flash('success', 'Testimony successfully made');
    }
}
