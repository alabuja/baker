<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Post;
use App\Models\Activity;
use Illuminate\Support\Facades\Auth;

class CategoryController extends Controller
{
	public function __construct(Category $category, Post $post, Activity $activity)
	{
        $this->category = $category;
		$this->post = $post;
        $this->activity         =   $activity;
	}

    public function show()
    {
        $categories = $this->category->categories();

        return view('admin.category.show', compact('categories'));
    }

    public function store(Request $request)
    {
    	$this->validate($request, [
    		'title' => 'required'
    	]);


    	$this->category->title = $request->title;
    	$this->category->save();

    	$request->session()->flash('success', 'Category added successfully');

    	return back();
    }

    public function category($slug)
    {
        $categories     =   $this->category->categories();
        $category = Category::where('slug', $slug)->first();

        $activities = '';

        if(Auth::check()){

            $activities     =   $this->activity->activities()->take(5);
        }

        $categoryId = $category->id;

        $results =  [ 
                   
                   'posts'   =>     $this->post->getPostsByCategeory($slug),
                   'posts'   =>     Post::where('category_id', $categoryId)->paginate(30),
               ];

        return view('category.posts', compact('categories', 'activities'), $results);
    }
    
}
