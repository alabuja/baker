<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Post;
use App\Models\Comment;
use App\Models\Activity;
use App\Models\Category;
use Illuminate\Pagination\LengthAwarePaginator;

class PostController extends Controller
{
	public function __construct(Post $post, Activity $activity, Category $category, Comment $comment)
	{
		$this->post       = $post;
        $this->activity   =   $activity;
        $this->category   = $category;
        $this->comment    = $comment;
	}

    public function create()
    {
        $categories = $this->category->categories();

        return view('post.new', compact('categories'));
    }

    public function store(Request $request)
    {
    	$this->validate($request, [
    		'title'       	=> 	'required',
    		'category_id' 	=> 	'required|numeric',
    		'body'			=>	'required'

    	]);

    	$userId = Auth::user()->id;

    	$this->post->title =  $request->title;
    	$this->post->category_id = $request->category_id;
    	$this->post->user_id = $userId;
    	$this->post->body = $request->body;
    	$this->post->post_type = 'earnable';

    	$this->post->save();

    	$request->session()->flash('success', 'Post Created Successfully');

    	return back();
    }

    public function post($slug)
    {
        $post = Post::select('*')->where('slug', $slug)->first();
        
        $activities = '';

        if(Auth::check()){

            $activities     =   $this->activity->activities()->take(5);
        }

        $postId = $post->id;
        $post->increment('visitCount');

        $viewCount = $post->visitCount;

        $lastComment = $this->comment->lastComment($slug);

        $results =  [ 
                   
                   'comments'   =>     $this->post->getCommentsByPost($slug),
                   'comments'   =>     Comment::where('post_id', $postId)->paginate(30),
               ];

        return view('post.single', $results, [
            'post'      =>  $post,
            'activities' => $activities,
            'viewCount' =>  $viewCount,
            'lastComment' => $lastComment
        ]);
    }

    public function show()
    {
        $posts = $this->post->allPosts();

        return view('post.lists', compact('posts'));
    }

    public function approvePosts(Request $request, $postId)
    {
        $this->post::where('id', $postId)->update(['isApproved' => true]);
        
        $post = $this->post::where('id', $postId)->first();
        $userId = $post->userId;

        $activity = new Activity;
        $activity->user_id       =  $userId;
        $activity->activity_type = 'posts';
        $activity->amount_earned = '50';
        $activity->save();
        
        $request->session()->flash('success', 'Post has been approved');

        return back();
    }

    public function rejectPosts(Request $request, $postId)
    {
        $this->post::where('id', $postId)->update(['isApproved' => false]);

        $request->session()->flash('success', 'Post has been rejected!');

        return back();
    }

    public function deletePosts(Request $request, $postId)
    {
        $this->post::where('id', $postId)->delete();

        $request->session()->flash('success', 'Post has been deleted!');

        return back();
    }
}
