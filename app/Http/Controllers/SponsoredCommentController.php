<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\SponsoredComment;
use App\Models\Activity;
use App\Models\SponsoredPost;

class SponsoredCommentController extends Controller
{
    public function __construct(SponsoredComment $comment, Activity $activity, SponsoredPost $sponsoredPost)
	{
		$this->comment = $comment;
        $this->activity = $activity;
		$this->sponsoredPost = $sponsoredPost;
	}

	public function store(Request $request, $slug)
    {
    	$this->validate($request, [
    		'body'				=>	'required'
    	]);

        $post = SponsoredPost::where('slug', $slug)->first();
        $postId = $post->id;

    	$userId = Auth::user()->id;

    	$this->comment->sponsored_post_id  	= 	$postId;
    	$this->comment->body  				=	$request->body;
    	$this->comment->user_id				=	$userId;
    	$this->comment->save();

    	$count = $this->comment->where('user_id', $userId)->where('sponsored_post_id', $postId)->count();

    	if($count <= 1)
    	{
    		$this->activity->user_id       =  $userId;
        	$this->activity->activity_type = 'sponsored';
        	$this->activity->amount_earned = '50';
        	$this->activity->save();
    	}

    	$request->session()->flash('success', 'Comment successfully made');

        return back();
    }

    public function edit($slug, $commentId)
    {
        $post = SponsoredPost::where('slug', $slug);

        $comment = $this->comment::where('id', $commentId)
                                ->first();

        if ($comment) {
            $return =   [
                            "status"    =>  200,
                            "response"  =>  $comment
                        ];

            return json_encode($return);
        }
    }

    public function update(Request $request, $slug, $commentId)
    {
        $this->validate($request,[
            'body' => 'required'
        ]);

        $body = $request->body;

        $post = SponsoredPost::where('slug', $slug)->first();
        $postId = $post->id;

        $this->comment::where('id', $commentId)
                        ->where('sponsored_post_id', $postId)
                        ->update(['body' => $body]);

        $request->session()->flash('success', 'Comment successfully made.');

        return back();
    }
}
