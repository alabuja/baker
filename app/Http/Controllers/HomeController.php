<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Post;
use App\Models\SponsoredPost;
use App\Models\Activity;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Category $category, Post $post, SponsoredPost $sponsoredPost, Activity $activity)
    {
        $this->category         =   $category;
        $this->post             =   $post;
        $this->sponsoredPost    =   $sponsoredPost;
        $this->activity         =   $activity;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories     =   $this->category->categories();
        $pinPost       =   $this->post->pinPosts();
        $sponsoredPost  =   $this->sponsoredPost->sponsoredPost();

        $activities = '';

        if(Auth::check()){

            $activities     =   $this->activity->activities()->take(5);
        }

        $results =     [ 
                   'posts'  =>  $this->post->posts(),
                   'posts'  => Post::select("*")->orderBy('created_at', 'desc')->paginate(30),
               ];

        return view('home', $results, compact('categories', 'pinPost', 'sponsoredPost', 'activities'));
    }

    public function money()
    {
        return view('money');
    }

    public function advertisement()
    {
        return view('advertisement');
    }

    public function contact()
    {
        return view('contact');
    }

    public function faq()
    {
        return view('faq');
    }

    public function terms()
    {
        return view('terms');
    }

    public function privacy()
    {
        return view('privacy');
    }

    public function about()
    {
        return view('about');
    }
}
