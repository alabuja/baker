<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\PaymentRequest;
use App\Models\Activity;
use App\Models\User;
use App\Models\Referral;
use Carbon\Carbon;

class PaymentRequestController extends Controller
{
	public function __construct(PaymentRequest $paymentRequest, Activity $activity, Referral $referral, User $user)
	{
        $this->paymentRequest   = $paymentRequest;
        $this->activity         = $activity;
        $this->referral         = $referral;
		$this->user             = $user;
	}

    public function store(Request $request)
    {
    	$this->validate($request, [
    		'amount_requested' => 'required'
    	]);

        $currentAmount      =   $this->activity->currentActivityEarnings();

        if($request->amount_requested != $currentAmount)
        {
            $this->paymentRequest->amount_requested = $currentAmount;
        }
        else
        {
            $this->paymentRequest->amount_requested = $request->amount_requested;
        }

    	$userId = Auth::user()->id;

    	$this->paymentRequest->user_id = $userId;

    	$this->paymentRequest->save();

    	$request->session()->flash('success', 'Your request is being processed');

    	return back();
    }

    public function approve(Request $request, $id)
    {
        $this->validate($request, [
            'amount_paid'   => 'required',
            'referralCount' => 'required'
        ]);

        $amountPaid     =   $request->amount_paid;
        $referralCount  =   $request->referralCount;

        $newPaymentRequest  = $this->paymentRequest->find($id);
        $userId             = $newPaymentRequest->user_id;

        if($referralCount > 0)
        {
            $getUserReferral = $this->referral->where('user_id', $userId)->where('package_type', 'Affiliate')->take($referralCount)->get();
            
            foreach ($getUserReferral as $key => $user) {
                    
                if ($amountPaid >= 700) {
                    $newUserId = $user->user_id;

                    $this->referral::where('user_id', $newUserId)->delete();
                }
                $amountPaid = $amountPaid - 700;
            }
        }

        $this->paymentRequest::where('id', $id)
                            ->update(['isApprove' => true, 'isPending' => true, 'amount_paid' => $request->amount_paid]);

        $request->session()->flash('success', 'This request has been approved');

        return back();
    }

    public function unApprove(Request $request, $id)
    {
        $this->paymentRequest::where('id', $id)
                            ->update(['isApprove' => false, 'isPending' => true]);

        $request->session()->flash('success', 'This request has been approved');

        return back();
    }

    public function show()
    {
    	$paymentRequests  =  $this->paymentRequest->allPendingRequests();

        // dd($paymentRequests);

    	return view('admin.payment_request', compact('paymentRequests'));
    }
}
