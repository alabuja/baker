<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\SponsoredPost;
use App\Models\SponsoredComment;
use App\Models\Activity;
use Cloudder;
use Illuminate\Pagination\LengthAwarePaginator;

class SponsoredPostController extends Controller
{
    public function __construct(SponsoredPost $sponsoredPost, Activity $activity, SponsoredComment $sponsoredComment)
    {
    	$this->sponsoredPost = $sponsoredPost;
        $this->activity         =   $activity;
        $this->sponsoredComment         =   $sponsoredComment;
    }

    public function show()
    {
        $posts = $this->sponsoredPost->mySponsoredPost();

        return view('sponsored_post.lists', compact('posts'));
    }

    public function create()
    {
        return view('sponsored_post.new');
    }

    public function store(Request $request)
    {
    	$this->validate($request, [
    		'title' => 'required',
    		'body'	=>	'required',
            'image' =>  'required|mimes:jpeg,jpg,png',
    	]);

        $userId = Auth::guard('admin')->user()->id;

        $image                          =   $request->file('image')->getRealPath();

        Cloudder::upload($image, null);
        list($width, $height)                = getimagesize($image);

        $url = Cloudder::secureShow(Cloudder::getPublicId(), [
            "crop" => "fit", "width" => 200, "height" => 200
        ]);


    	$this->sponsoredPost->title = $request->title;
        $this->sponsoredPost->admin_id = $userId;
        $this->sponsoredPost->body = $request->body;
    	$this->sponsoredPost->image_url = $url;
    	$this->sponsoredPost->save();

    	$request->session()->flash('success', 'Sponsored Post Created Successfully');

    	return back();
    }

    public function sponsoredPost($slug)
    {
        $post = SponsoredPost::where('slug', $slug)->first();
        
        $activities = '';

        if(Auth::check()){

            $activities     =   $this->activity->activities()->take(5);
        }

        $postId = $post->id;
        $post->increment('visitCount');

        $viewCount = $post->visitCount;

        $lastComment = $this->sponsoredComment->lastComment($slug);

        $results =  [ 
                   
            'comments'   =>   $this->sponsoredPost->getCommentsBySponsoredPost($slug),
            'comments'   =>   SponsoredComment::where('sponsored_post_id', $postId)->paginate(30),
        ];

        return view('sponsored_post.single', $results, compact('post', 'activities', 'lastComment', 'viewCount'));
    }
}
