<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSponsoredPostsCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sponsored_posts_comments', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('sponsored_post_id');
            $table->unsignedInteger('user_id');
            $table->longText('body');
            $table->timestamps();


            $table->foreign('sponsored_post_id')->references('id')->on('sponsored_posts');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sponsored_posts_comments');
    }
}
