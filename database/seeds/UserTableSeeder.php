<?php

use App\Models\Permission;
use App\Models\Role;
use App\Models\Admin;

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dev_role = Role::where('slug','developer')->first();
	    $manager_role = Role::where('slug', 'manager')->first();
	    $dev_perm = Permission::where('slug','create-tasks')->first();
	    $manager_perm = Permission::where('slug','edit-tasks')->first();

	    $developer = new Admin();
	    $developer->name = 'Jonathan Jeremiah';
	    $developer->email = 'usama@protech.studio';
	    $developer->password = bcrypt('password');
	    $developer->save();

	    $developer->roles()->attach($dev_role);
	    $developer->permissions()->attach($dev_perm);

	    $manager = new Admin();
	    $manager->name = 'Baker Olanrewaju';
	    $manager->email = '';
	    $manager->password = bcrypt('password');
	    $manager->save();

	    $manager->roles()->attach($manager_role);
	    $manager->permissions()->attach($manager_perm);
    }
}
