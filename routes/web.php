<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Auth::routes();

Route::get('/', 'HomeController@index');

Route::group(['middleware' => ['guest']], function(){
	Route::get('users/login', 'Auth\LoginController@getLoginForm');
	Route::post('users/authenticate', 'Auth\LoginController@authenticate');

	Route::get('register', 'Auth\RegisterController@showRegistrationForm');
	Route::post('register', 'Auth\RegisterController@register');
	Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
	Route::post('password/reset', 'Auth\ResetPasswordController@reset');
	Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm');
	Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');
});

Route::group(['prefix' => 'admin', 'middleware' => ['admin.guest']], function(){

	// ADMIN
    Route::get('login', 'Admin\Auth\LoginController@getLoginForm');
    Route::post('authenticate', 'Admin\Auth\LoginController@authenticate');
    Route::post('password/email', 'Admin\Auth\ForgotPasswordController@sendResetLinkEmail');
  	Route::post('password/reset', 'Admin\Auth\ResetPasswordController@reset');
    Route::get('password/reset', 'Admin\Auth\ForgotPasswordController@showLinkRequestForm');
    Route::get('password/reset/{token}', 'Admin\Auth\ResetPasswordController@showResetForm');
	
});


Route::group(['prefix' => 'admin', 'middleware' => ['admin']], function(){

	Route::get('dashboard', 'AdminController@dashboard');
	Route::get('password', 'AdminController@password');
	Route::get('profile', 'AdminController@profile');
	Route::post('password', 'AdminController@updatePassword');
	Route::post('profile', 'AdminController@update');

	Route::post('category/new', 'CategoryController@store');
	Route::get('categories', 'CategoryController@show'); // Modal to Create the Categories
	Route::get('payments', 'PaymentController@show');
	Route::post('paymentRequests/approve/{id}', 'PaymentRequestController@approve');
	Route::post('paymentRequests/reject/{id}', 'PaymentRequestController@unApprove');
	Route::get('paymentRequests', 'PaymentRequestController@show');
	Route::get('sponsor/posts', 'SponsoredPostController@show');
	Route::get('posts', 		'PostController@show');
	Route::get('sponsor/new', 'SponsoredPostController@create');
	Route::post('sponsor/new', 'SponsoredPostController@store');
	Route::get('set', 'WithdrawController@setWithDrawal');
	Route::post('set/open', 'WithdrawController@open');
	Route::post('set/close', 'WithdrawController@close');
	Route::get('posts/create', 'AdminController@create');
	Route::post('posts/create', 'AdminController@store');

	Route::post('posts/approve/{id}', 'PostController@approvePosts');
	Route::post('posts/reject/{id}', 'PostController@rejectPosts');
	Route::post('posts/delete/{id}', 'PostController@deletePosts');

	Route::get('users/{id}', 'AdminController@viewUser');
	Route::get('users', 'AdminController@viewUsers');

	Route::post('logout',                   'Admin\Auth\LoginController@logout');
});

Route::group(['prefix' => 'users', 'middleware' =>  ['auth']], function(){

	Route::get('upgrade', 'UserController@upgrade');
	Route::get('withdraw', 'UserController@withdraw');
	Route::get('referrals', 'UserController@myReferrals');
	Route::get('timeline', 'UserController@timeline');
	Route::get('profile', 'UserController@profile');
	Route::post('profile', 'UserController@update');
	Route::get('dashboard', 'UserController@dashboard');
	Route::get('activities', 'ActivityController@show');
	Route::get('password', 'UserController@password');
	Route::post('password', 'UserController@updatePassword');
	Route::get('bankInfo', 'BankInfoController@bankInfo');
	Route::post('bankInfo', 'BankInfoController@update');
	Route::post('withdraw/paymentRequests', 'PaymentRequestController@store');
	Route::post('posts', 'PostController@store');
	Route::post('testimony', 'TestimonyController@store');
	Route::get('post/new', 'PostController@create');
	Route::post('post/new', 'PostController@store');

	Route::post('logout',                   'Auth\LoginController@logout');
});

Route::group(['middleware' =>  ['auth']], function(){

	Route::get('sponsor/post', 'UserController@sponsoredPost');
	Route::get('sponsor/post/{slug}', 'SponsoredPostController@sponsoredPost');

	Route::post('topic/comment/{slug}', 'CommentController@store');
	Route::get('forum/topic/{slug}/comment/{commentId}', 'CommentController@edit');
	Route::put('forum/update/{slug}/comment/{commentId}', 'CommentController@update');

	Route::post('sponsor/comment/{slug}', 'SponsoredCommentController@store');
	Route::get('sponsor/post/{slug}/comment/{commentId}', 'SponsoredCommentController@edit');
	Route::put('sponsor/post/{slug}/comment/{commentId}', 'SponsoredCommentController@update');

	Route::post('pay', 'PaymentController@redirectToGateway');
	Route::get('payment/callback', 'PaymentController@handleGatewayCallback');
});


Route::get('forum/topic/{slug}', 'PostController@post');
Route::get('forum/{slug}', 'CategoryController@category');

Route::get('referral-register/{slug}/{userId}', 'ReferralController@getReferral');
Route::post('referral-register/{slug}/{userId}', 'ReferralController@store');

Route::get('how-to-make-money', 'HomeController@money');
Route::get('contact', 'HomeController@contact');
Route::get('faq', 'HomeController@faq');
Route::get('about', 'HomeController@about');
Route::get('terms', 'HomeController@terms');
Route::get('advertisement', 'HomeController@advertisement');
Route::get('privacy', 'HomeController@privacy');
